/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.component.geometry;

import co.cata.ecs.engine.Component;
import co.cata.ecs.renderer.Mesh;
import co.cata.ecs.renderer.Shader;
import co.cata.ecs.renderer.Texture;

public class GeometryComponent extends Component {

    public Mesh mesh;
    public Texture texture;
    public Shader shader;
    
    public GeometryComponent() {
        mesh = null;
        texture = null;
        shader = null;
    }
    
    public boolean isValid() {
        if(mesh == null || texture == null) {
            return false;
        }
        return true;
    }
}
