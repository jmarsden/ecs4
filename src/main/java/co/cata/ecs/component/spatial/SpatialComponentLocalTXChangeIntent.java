/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.component.spatial;

import co.cata.ecs.engine.Component;
import co.cata.ecs.engine.ComponentIntent;

public class SpatialComponentLocalTXChangeIntent extends ComponentIntent {

    float x;

    float xDelta;
	
	public SpatialComponentLocalTXChangeIntent(Component source, float x, float xDelta) {
		super(source);
		this.x = x;
		this.xDelta = xDelta;
	}

	/**
	 * @return the x
	 */
	public float getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * @return the xDelta
	 */
	public float getxDelta() {
		return xDelta;
	}

	/**
	 * @param xDelta the xDelta to set
	 */
	public void setxDelta(float xDelta) {
		this.xDelta = xDelta;
	}
    
    /* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SpatialComponentLocalTXChangeIntent [x=" + x + ", delta=" + xDelta + "]";
	}
}
