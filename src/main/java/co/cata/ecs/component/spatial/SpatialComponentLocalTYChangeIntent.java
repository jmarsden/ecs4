/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.component.spatial;

import co.cata.ecs.engine.Component;
import co.cata.ecs.engine.ComponentIntent;

public class SpatialComponentLocalTYChangeIntent extends ComponentIntent {

    float y;

    float yDelta;

    public SpatialComponentLocalTYChangeIntent(Component source, float y, float yDelta) {
        super(source);
        this.y = y;
        this.yDelta = yDelta;
    }

    /**
     * @return the y
     */
    public float getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(float y) {
        this.y = y;
    }

    /**
     * @return the yDelta
     */
    public float getyDelta() {
        return yDelta;
    }

    /**
     * @param yDelta the yDelta to set
     */
    public void setyDelta(float yDelta) {
        this.yDelta = yDelta;
    }
    
    /* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SpatialComponentLocalTYChangeIntent [y=" + y + ", delta=" + yDelta + "]";
	}
}