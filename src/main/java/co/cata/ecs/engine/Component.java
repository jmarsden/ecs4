/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.engine;

import co.cata.ecs.renderer.Renderer;
import java.util.ArrayList;
import java.util.List;

public abstract class Component implements ComponentIntentRecipient {

    protected GameObject gameObject;

    List<Integer> intentTopics;

    public Component() {
        this.gameObject = null;
        this.intentTopics = new ArrayList<Integer>();
    }

    /**
     * @return the messageTopics
     */
    public int[] getIntentTopics() {
        int[] topicArray = new int[intentTopics.size()];
        for(int i = 0; i < topicArray.length; i++) {
            topicArray[i] = intentTopics.get(i);
        }
        return topicArray;
    }

    public void listenToTopic(int topic) {
        if(!intentTopics.contains(topic)) {
            intentTopics.add(topic);
        }
    }

    public boolean isListening() {
        return intentTopics != null;
    }

    public boolean listensToTopic(int topic) {
        for(int i = 0; i < intentTopics.size(); i++) {
            if(intentTopics.get(i) == topic) {
                return true;
            }
        }
        return false;
    }

    protected boolean activeIntentTopicRecipients(int topic) {
        // TODO : Optimise this.
        return true;
    }

    protected void sendIntent(ComponentIntent intent) {
        if(gameObject != null) {
            List<Component> listeners = gameObject.findIntentTopicRecipients(intent.topic);
            for(Component component : listeners) {
                component.recieveIntent(intent);
                intent.addReceiver(component);
            }
            if(gameObject.logMessages && intent.wasReceived()) {
                System.out.println("I:" + intent);
            }
        }
    }

    public void recieveIntent(ComponentIntent intent) {
    }

    public void init() {
    }

    public void start() {
    }

    public void update(float delta) {
    }

//    public void render(Renderer renderer) {
//    }

    @Override
    public String toString() {
        return gameObject + ":[" + getClass().getSimpleName() + "]";
    }
}
