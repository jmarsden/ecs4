/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.engine;

import java.util.ArrayList;
import java.util.List;

public abstract class ComponentIntent {

    public Component source;
    public List<Component> receivers;
    public int topic;

    public ComponentIntent(Component source) {
        this.source = source;
        this.receivers = null;
        this.topic = -1;
    }

    /**
     * @return the source
     */
    public Component getSource() {
        return source;
    }

    /**
     * @param topic the topic to set
     */
    public void setTopic(int topic) {
        this.topic = topic;
    }

    /**
     * @return the topic
     */
    public int getTopic() {
        if (topic == -1) {
            topic = IntentRegistry.lookupSingleton().registerIntent(this);
        }
        return topic;
    }

    public void addReceiver(Component component) {
        if (receivers == null) {
            receivers = new ArrayList<Component>();
        }
        receivers.add(component);
    }

    public boolean wasReceived() {
        if (receivers != null && !receivers.isEmpty()) {
            return true;
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        //[Receivers=\"" + ((receivers == null) ? 0 : receivers.size()) + "\"]
        return "ComponentIntent [" + getClass().getSimpleName() + " Source=" + source + " Topic=" + getTopic() + "]";
    }
}
