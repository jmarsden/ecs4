/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.engine;

import co.cata.ecs.component.spatial.SpatialComponent;
import co.cata.ecs.renderer.Renderer;
import java.util.ArrayList;
import java.util.List;

public class Engine {

    private int idCounter;
    List<GameObject> objects;
    private static Engine engineInstance;

    static {
        engineInstance = null;
    }

    public Engine() {
        idCounter = 0;
        objects = new ArrayList<GameObject>();
    }

    public Node createNode() {
        Node object = new Node(this, nextId());
        object.setName("Node:" + object.getId());
        SpatialComponent spatial = new SpatialComponent();
        object.registerComponent(spatial);
        objects.add(object);
        return object;
    }

    public Node createNode(String name) {
        Node object = new Node(this, nextId());
        object.setName(name);
        SpatialComponent spatial = new SpatialComponent();
        object.registerComponent(spatial);
        objects.add(object);
        return object;
    }

    public GameObject createGameObject() {
        GameObject object = new GameObject(this, nextId());
        object.setName("Game Object:" + object.getId());
        SpatialComponent spatial = new SpatialComponent();
        object.registerComponent(spatial);
        objects.add(object);
        return object;
    }

    public GameObject createGameObject(String name) {
        GameObject object = new GameObject(this, nextId());
        object.setName(name);
        SpatialComponent spatial = new SpatialComponent();
        object.registerComponent(spatial);
        objects.add(object);
        return object;
    }

    void markChildGameObject(GameObject child) {
        objects.remove(child);
    }

    void unmarkChildGameObject(GameObject child) {
        objects.add(child);
    }

    /*
     public GameObject createGameObject(String name) {
     GameObject object = new GameObject(nextId());
     object.setName(name);
     objects.add(object);
     return object;
     }
     */
    int nextId() {
        return idCounter++;
    }

    public void init() {
        for (GameObject object : objects) {
            object.init();
        }
    }

    public void start() {
        for (GameObject object : objects) {
            object.start();
        }
    }

    public void update(float delta) {
        for (GameObject object : objects) {
            if (!object.hasParent()) {
                object.update(delta);
            }
        }
    }

    public void render(Renderer renderer) {
        renderer.startRender();
        for (GameObject object : objects) {
            object.render(renderer);
        }
        renderer.completeRender();
    }

    public String getState() {
        StringBuilder builder = new StringBuilder();
        builder.append("Engine State (").append(objects.size()).append(" Roots)");
        int count = 0;
        for (GameObject object : objects) {
            dumpState(builder, object, 0, "-" + count);
            count++;
        }
        return builder.toString();
    }

    public void dumpState() {
        StringBuilder builder = new StringBuilder();
        builder.append("Engine State (").append(objects.size()).append(" Roots)");
        int count = 0;
        for (GameObject object : objects) {
            dumpState(builder, object, 0, "-" + count);
            count++;
        }
        System.out.println(builder.toString());
    }

    public void dumpState(StringBuilder builder, GameObject object, int level, String prefix) {
        List<GameObject> children = object.children;
        builder.append('\r').append('\n');
        for (int i = 0; i < level; i++) {
            builder.append("  ");
        }
        builder.append(prefix).append(" ").append(object);
        String newPrefix;
        for (int i = 0; i < children.size(); i++) {
            GameObject child = children.get(i);
            newPrefix = prefix + "." + i;
            dumpState(builder, child, level + 1, newPrefix);
        }
    }

    public synchronized static void initEngine() {
        if (engineInstance != null) {
            throw new RuntimeException("Cannot Init Engine - It has already been done!");
        }
        engineInstance = new Engine();
    }

    public static Engine getInstance() {
        if (engineInstance == null) {
            initEngine();
        }
        return engineInstance;
    }
}
