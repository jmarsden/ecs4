/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.provider;

import co.cata.ecs.renderer.Texture;
import co.cata.utils.PNGDecoder;
import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;

public class FileLWJGLTexture extends Texture {
    
    public URL url;
    
    public FileLWJGLTexture(String path) {
        this.url = ClassLoader.class.getResource(path);
        this.textureHandle = -1;
        this.imageBuffer = null;
    }

    public FileLWJGLTexture(URL url) {
        this.url = url;
        this.textureHandle = -1;
        this.imageBuffer = null;
    }
    
    public boolean isInitialized() {
        return imageBuffer != null;
    }

    public void init() {
        try {
            // Open the PNG file as an InputStream
            //URL file = ClassLoader.class.getResource(path);

            // Link the PNG decoder to this stream
            PNGDecoder decoder = new PNGDecoder(url.openStream());

            // Get the width and height of the texture
            textureWidth = decoder.getWidth();
            textureHeight = decoder.getHeight();

            // Decode the PNG file in a ByteBuffer
            imageBuffer = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());
            decoder.decode(imageBuffer, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
            imageBuffer.flip();
        } catch (IOException e) {
            throw new RuntimeException("Clean this up:" + e);
        }
    }

    public boolean isLoaded() {
        return textureHandle != -1;
    }

    public void load() {
        if (isLoaded()) {
            throw new RuntimeException("Already Loaded");
        }
        if(!isInitialized()) {
            init();
        }

        // Create a new texture object in memory and bind it
        textureHandle = GL11.glGenTextures();
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureHandle);

        // All RGB bytes are aligned to each other and each component is 1 byte
        GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);

        // Upload the texture data and generate mip maps (for scaling)
        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, textureWidth, textureHeight, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, imageBuffer);
        GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);

        // Setup the ST coordinate system
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);

        // Setup what to do when the texture has to be scaled
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
    }

    public void unLoad() {
        if (textureHandle != -1) {
            GL11.glDeleteTextures(textureHandle);
            textureHandle = -1;
        }
    }

    public void release() {
        if (isLoaded()) {
            unLoad();
        }
        if (imageBuffer != null) {
            imageBuffer = null;
        }
    }
}
