/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.provider;

import co.cata.ecs.runtime.ApplicationInterface;
import java.util.logging.Logger;
import org.lwjgl.glfw.GLFW;

public class LWJGL3Application {

    public static final Logger LOGGER = Logger.getLogger(LWJGL3Application.class.getCanonicalName());
    long lastFrame;
    int fps;
    long lastFPS;
    boolean pauseFlag;
    ApplicationInterface applicationInterface;
    LWJGL3ApplicationConfiguration applicationConfiguration;
    LWJGL3Renderer renderer;

    public LWJGL3Application(ApplicationInterface applicationInterface, LWJGL3ApplicationConfiguration applicationConfiguration) {
        if (applicationInterface == null) {
            throw new NullPointerException("applicationInterface cannot be null.");
        }
        if (applicationConfiguration == null) {
            throw new NullPointerException("applicationConfiguration cannot be null.");
        }
        this.applicationInterface = applicationInterface;
        this.applicationConfiguration = applicationConfiguration;
        pauseFlag = false;

    }

    public void start() {
        int width = applicationConfiguration.width;
        int height = applicationConfiguration.height;
        LWJGL3Display display = new LWJGL3Display();
        display.setTitle(applicationConfiguration.title);
        display.setResizable(applicationConfiguration.resizable);
        display.init(width, height);

        renderer = (LWJGL3Renderer) display.createRenderer(applicationConfiguration);
        renderer.backgroundColor = applicationConfiguration.backGroundColor;
        renderer.init();

        applicationInterface.reSizeCallback(width, height);
        applicationInterface.init(renderer);
        applicationInterface.start();

        getDelta();
        lastFPS = getTime();

        while (!renderer.display.isCloseRequested()) {
            float delta = getDelta();

            applicationInterface.update(delta);
            
//            if(!renderer.display.isActive()) {
//                applicationInterface.pause();
//                pauseFlag = true;
//            } else {
//                applicationInterface.resume();
//                pauseFlag = false;
//                
//            }
            applicationInterface.render(renderer);
            
//            if (!renderer.display.isActive()) {
//                if (!pauseFlag) {
//                    applicationInterface.pause();
//                    pauseFlag = true;
//                }
//            } else {
//                if (pauseFlag) {
//                    applicationInterface.resume();
//                    pauseFlag = false;
//                }
//
//                int newWidth = renderer.display.getWidth();
//                int newHeight = renderer.display.getHeight();
//                if (width != newWidth || height != newHeight) {
//                    width = newWidth;
//                    height = newHeight;
//                    this.renderer.display.reSize(width, height);
//                    applicationInterface.reSizeCallback(width, height);
//                }
                
//            }
//            renderer.display.sync(renderer.frameRate);
              renderer.display.update();

            updateFPS();
        }

        applicationInterface.exit();
        renderer.display.destroy();
    }

    /**
     * Calculate how many milliseconds have passed since last frame.
     *
     * @return milliseconds passed since last frame
     */
    public float getDelta() {
        long time = getTime();
        float delta = (float) (time - lastFrame);
        lastFrame = time;
        return delta;
    }

    /**
     * Get the accurate system time
     *
     * @return The system time in milliseconds
     */
    public long getTime() {
        return (long)(GLFW.glfwGetTime() * 1000);
    }

    public void updateFPS() {
        if (getTime() - lastFPS > 1000) {
            applicationInterface.fpsUpdateCallback(renderer, fps);
            fps = 0;
            lastFPS += 1000;
        }
        fps++;
    }
}
