/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.provider;

import co.cata.ecs.renderer.Extensions;
import co.cata.ecs.renderer.GLRenderer;
import co.cata.ecs.renderer.GLSLVersion;
import co.cata.ecs.renderer.GLVendor;
import co.cata.ecs.renderer.GLVersion;
import co.cata.ecs.renderer.JavaVersion;
import co.cata.ecs.renderer.Platform;
import co.cata.ecs.renderer.Provider;
import co.cata.ecs.renderer.Renderer;
import co.cata.ecs.runtime.ApplicationConfiguration;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.Version;
import org.lwjgl.glfw.GLFW;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwSetWindowSize;
import static org.lwjgl.glfw.GLFW.glfwSetWindowSizeCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowTitle;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import org.lwjgl.opengl.GLCapabilities;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 *
 * @author jmarsden
 */
public class LWJGL3Display implements co.cata.ecs.renderer.Display {

    private final Object lock;

    private GLCapabilities capabilities;
    private GLFWErrorCallback errorCallback;
    private GLFWKeyCallback keyCallback;
    private GLFWWindowSizeCallback windowSizeCallback;

    // The window handle
    private long window;
    private String title;
    private int width;
    private int height;
    private boolean resizable;

    public boolean displayInit;
    public boolean displayCreated;
    public boolean displayVisible;

    public JavaVersion javaVersion;
    public Platform platform;
    public Provider provider;
    public GLVendor glVendor;
    public GLRenderer glRenderer;
    public GLVersion glVersion;
    public GLSLVersion glslVersion;
    public Extensions extensions;

    static int DEFAULT_WIDTH = 300;
    static int DEFAULT_HEIGHT = 300;

    public LWJGL3Display() {
        displayInit = false;
        displayCreated = false;
        displayVisible = false;
        title = "Display:" + getClass().getName();

        lock = new Object();
        width = DEFAULT_WIDTH;
        height = DEFAULT_HEIGHT;
        resizable = false;
    }

    @Override
    public void init(int width, int height) {
        glfwSetErrorCallback(errorCallback = GLFWErrorCallback.createPrint(System.err));
        synchronized (lock) {
            this.width = width;
            this.height = height;
            try {
                if (glfwInit() != GL11.GL_TRUE) {
                    throw new IllegalStateException("Unable to initialize GLFW");
                }

                glfwDefaultWindowHints();
                glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
                glfwWindowHint(GLFW_RESIZABLE, (resizable) ? GL_TRUE : GL_FALSE);

                create();
            } catch (Exception ex) {
                Logger.getLogger(LWJGL3Display.class.getName()).log(Level.SEVERE, null, ex);
            }

            glfwSetKeyCallback(window, keyCallback = new GLFWKeyCallback() {
                @Override
                public void invoke(long window, int key, int scancode, int action, int mods) {
                    if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
                        glfwSetWindowShouldClose(window, GL_TRUE);
                    }
                }
            });
            javaVersion = new JavaVersion(System.getProperty("java.version"));
            provider = new Provider("LWJGL", Version.getVersion());

            glfwSetWindowSizeCallback(window, windowSizeCallback = new GLFWWindowSizeCallback() {

                @Override
                public void invoke(long window, int width, int height) {
                    reSizeEvent(width, height);
                }
            });

//            switch (LWJGLUtil.getPlatform()) {
//                case LINUX:
//                    platform = new Platform(Platform.LINUX, Platform.LINUX_NAME);
//                    break;
//                case MACOSX:
//                    platform = new Platform(Platform.OSX, Platform.OSX_NAME);
//                    break;
//                case WINDOWS:
//                    platform = new Platform(Platform.WINDOWS, Platform.WINDOWS_NAME);
//                    break;
//            }
            platform = new Platform(Platform.UNKOWN, Platform.UNKOWN_NAME);

            glVendor = new GLVendor(GL11.glGetString(GL11.GL_VENDOR));
            glRenderer = new GLRenderer(GL11.glGetString(GL11.GL_RENDERER));

            String versionString = GL11.glGetString(GL11.GL_VERSION);
            String versionStringSuffix = versionString.substring(0, 2);
            double versionDouble = Double.parseDouble(versionStringSuffix);
            if (versionDouble == 1.1D) {
                glVersion = GLVersion.GL11;
                glslVersion = GLSLVersion.GLSLNONE;
            } else if (versionDouble == 1.2D) {
                glVersion = GLVersion.GL12;
                glslVersion = GLSLVersion.GLSLNONE;
            } else if (versionDouble == 1.3D) {
                glVersion = GLVersion.GL13;
                glslVersion = GLSLVersion.GLSLNONE;
            } else if (versionDouble == 1.4D) {
                glVersion = GLVersion.GL14;
                glslVersion = GLSLVersion.GLSLNONE;
            } else if (versionDouble == 1.5D) {
                glVersion = GLVersion.GL15;
                glslVersion = GLSLVersion.GLSLNONE;
            } else if (versionDouble == 2.0D) {
                glVersion = GLVersion.GL20;
                glslVersion = GLSLVersion.GLSL110;
            } else if (versionDouble == 2.1D) {
                glVersion = GLVersion.GL21;
                glslVersion = GLSLVersion.GLSL120;
            } else if (versionDouble == 3.0D) {
                glVersion = GLVersion.GL30;
                glslVersion = GLSLVersion.GLSL130;
            } else if (versionDouble == 3.1D) {
                glVersion = GLVersion.GL31;
                glslVersion = GLSLVersion.GLSL140;
            } else if (versionDouble == 3.2D) {
                glVersion = GLVersion.GL32;
                glslVersion = GLSLVersion.GLSL150;
            } else if (versionDouble == 3.3D) {
                glVersion = GLVersion.GL33;
                glslVersion = GLSLVersion.GLSL330;
            } else if (versionDouble == 4.0D) {
                glVersion = GLVersion.GL40;
                glslVersion = GLSLVersion.GLSL400;
            } else if (versionDouble == 4.1D) {
                glVersion = GLVersion.GL41;
                glslVersion = GLSLVersion.GLSL410;
            } else if (versionDouble == 4.2D) {
                glVersion = GLVersion.GL42;
                glslVersion = GLSLVersion.GLSL420;
            } else if (versionDouble == 4.3D) {
                glVersion = GLVersion.GL43;
                glslVersion = GLSLVersion.GLSL430;
            } else {
                glVersion = GLVersion.GLHMMN;
                glslVersion = GLSLVersion.GLSLNONE;
            }

            String glExtensions = GL11.glGetString(GL11.GL_EXTENSIONS);
            extensions = new Extensions(glExtensions);

            displayInit = true;
        }
    }

    public void reSize(int width, int height) {
        this.width = width;
        this.height = height;
        if (displayVisible) {
            synchronized (lock) {
                try {
                    glfwSetWindowSize(window, width, height);
                } catch (Exception ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
                GL11.glViewport(0, 0, width, height);
            }
        }
    }

    private void reSizeEvent(int width, int height) {
        this.width = width;
        this.height = height;
        if (displayVisible) {
            synchronized (lock) {
                GL11.glViewport(0, 0, width, height);
            }
        }
    }

    @Override
    public Renderer createRenderer(ApplicationConfiguration configuration) {
        if (!displayCreated) {
            throw new RuntimeException("Must init");
        }
        GLVersion version;
        if (configuration.forceGLVersion != null) {
            version = configuration.forceGLVersion;
        } else {
            version = glVersion;
        }
        if (version.getVersion() >= 1.5 && version.getVersion() < 2.0) {
            return new LWJGL3RendererGL15(this);
        } else if (version.getVersion() >= 2.0) {
            return new LWJGL3RendererGL2(this);
        } else {
            throw new RuntimeException("You must support OpenGL 2.0 Or Higher");
        }
    }

    private void create() {
        synchronized (lock) {
            window = glfwCreateWindow(this.width, this.height, "Hello World!", NULL, NULL);
            if (window == NULL) {
                throw new RuntimeException("Failed to create the GLFW window");
            }

            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
            glfwSetWindowPos(window, (vidmode.width() - this.width) / 2, (vidmode.height() - this.height) / 2);

            glfwSetWindowTitle(window, title);

            glfwMakeContextCurrent(window);
            capabilities = GL.createCapabilities();
            glfwSwapInterval(1);
            displayCreated = true;
            glfwShowWindow(window);
            displayVisible = true;
        }
    }

    public void setTitle(String title) {
        this.title = title;
        if (displayCreated) {
            glfwSetWindowTitle(window, title);
        }
    }

    public void setResizable(boolean resizable) {
        this.resizable = resizable;
        if (displayCreated) {
            glfwWindowHint(GLFW_RESIZABLE, resizable ? GL_TRUE : GL_FALSE);
        }
    }
    
    public void setIcon(ByteBuffer[] icons) {
        System.out.println("GLFW does not support setting a Window Icon Yet. To Be Added.");
    }

    public boolean isCloseRequested() {
        return glfwWindowShouldClose(window) == GL_TRUE;
    }

    public void update() {
        if (displayVisible) {
            glfwSwapBuffers(window);
            glfwPollEvents();
        }
    }

    public boolean isActive() {
        throw new RuntimeException("Remove");
    }

    public void sync(int fps) {
        throw new RuntimeException("Remove");
    }

    public void setVSyncEnabled(boolean sync) {
        throw new RuntimeException("Remove");
    }

    public boolean wasResized() {
        return false;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getWidth() {
        return width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void destroy() {

    }
}
