/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.provider;

import co.cata.ecs.component.spatial.SpatialComponent;
import co.cata.ecs.engine.GameObject;
import co.cata.ecs.renderer.Camera;
import co.cata.ecs.renderer.Extensions;
import co.cata.ecs.renderer.GLSLVersion;
import co.cata.ecs.renderer.GLVendor;
import co.cata.ecs.renderer.GLVersion;
import co.cata.ecs.renderer.Mesh;
import co.cata.ecs.renderer.OrthogonalProjection;
import co.cata.ecs.renderer.Platform;
import co.cata.ecs.renderer.Projection;
import co.cata.ecs.renderer.Provider;
import co.cata.ecs.renderer.RenderBatch;
import co.cata.ecs.renderer.RenderState;
import co.cata.ecs.renderer.Renderer;
import co.cata.ecs.renderer.Shader;
import co.cata.ecs.renderer.AxisCamera;
import co.cata.ecs.renderer.PerspectiveProjection;
import co.cata.ecs.renderer.Texture;
import co.cata.graphics.Vertex;
import co.cata.math.Matrix4f;
import co.cata.utils.Color;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import org.lwjgl.BufferUtils;

/**
 *
 * @author John
 */
public abstract class LWJGL3Renderer implements Renderer {

    public Logger logger;
    public Projection projection;
    public final Camera camera;
    public Color backgroundColor;
    public StringLWJGLShader defaultShader;
    public final Matrix4f projectionMatrix;
    public final Matrix4f viewMatrix;
    public final Matrix4f modelMatrix;
    public final LWJGL3Display display;
    public int frameRate;
    protected boolean lock;
    protected final RenderBatch batch;
    protected boolean skipRender;
    protected FloatBuffer mat4x4Buffer = null;

    public LWJGL3Renderer(LWJGL3Display display) {
        backgroundColor = Color.LIGHT_GREY;

        defaultShader = null;

        projectionMatrix = Matrix4f.identity();
        viewMatrix = Matrix4f.identity();
        modelMatrix = Matrix4f.identity();

        mat4x4Buffer = BufferUtils.createFloatBuffer(16);

        this.display = display;
        this.frameRate = 60;

        this.camera = new AxisCamera();

        lock = false;
        batch = new RenderBatch();

        logger = LogManager.getLogger(getClass().getName());
    }

    public abstract void init();

    public void setIcon(ByteBuffer[] icons) {
        if(icons != null) {
            display.setIcon(icons);
        }
    }
    
    public void setOrthoProjection(float left, float right, float top, float bottom) {
        projection = new OrthogonalProjection(left, right, top, bottom);
    }

    public void setOrthoProjection(float left, float right, float top, float bottom, float near, float far) {
        projection = new OrthogonalProjection(left, right, top, bottom, near, far);
    }

    public void setPerspectiveProjection(float near, float far, float fov, float aspectRatio) {
        projection = new PerspectiveProjection(near, far, fov, aspectRatio);
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public GLVersion getGLVersion() {
        if (display != null) {
            return display.glVersion;
        } else {
            return null;
        }
    }

    public GLSLVersion getGLSLVersion() {
        if (display != null) {
            return display.glslVersion;
        } else {
            return null;
        }
    }

    public GLVendor getGLVendor() {
        if (display != null) {
            return display.glVendor;
        } else {
            return null;
        }
    }

    public Platform getPlatform() {
        if (display != null) {
            return display.platform;
        } else {
            return null;
        }
    }

    public Provider getProvider() {
        if (display != null) {
            return display.provider;
        } else {
            return null;
        }
    }

    public Extensions getExtensions() {
        if (display != null) {
            return display.extensions;
        } else {
            return null;
        }
    }

    public Projection getProjection() {
        return projection;
    }

    public LWJGL3Display getDisplay() {
        return display;
    }

    public Camera getCamera() {
        return camera;
    }

    public abstract Shader createShader(String vertexSource, String fragmentSource);

    public abstract void releaseShader(Shader shader);

    public abstract Shader createDefaultShader(String vertexSource, String fragmentSource);

    public abstract Shader getDefaultShader();

    public Texture createTexture(String path) {
        return new FileLWJGLTexture(path);
    }

    public void releaseTexture(Texture texture) {
        texture.release();
    }

    public Mesh createStaticMesh(Vertex[] vertices, short[] indicies) {
        LWJGL3InMemoryMeshVAO mesh = new LWJGL3InMemoryMeshVAO(vertices, indicies);
        return mesh;
    }

    public Mesh createStaticMesh(Shader shader, Vertex[] vertices, short[] indicies) {
        LWJGL3InMemoryMeshVAO mesh = new LWJGL3InMemoryMeshVAO(vertices, indicies);
        return mesh;
    }

    public void releaseMesh(Mesh mesh) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setFrameRate(int frameRate) {
        this.frameRate = frameRate;
    }

    public void setVSyncEnabled(boolean vSync) {
        this.display.setVSyncEnabled(vSync);
    }

    public void startRender() {
        synchronized (this) {
            lock = true;
        }
    }

    public void addToRenderQueue(GameObject object) {
        if (lock == true) {
            batch.queue.add(object);
        } else {
            throw new RuntimeException("Render Not Started.");
        }
    }

    public boolean isRendering() {
        return lock;
    }

    public abstract void completeRender();

    public boolean cull(SpatialComponent spatial) {
        return false;
    }

    public void setRenderState(RenderState renderState) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Runtime Details").append('\n');
        builder.append("Java Runtime:\t").append(display.javaVersion).append('\n');

        builder.append("Platform:\t").append(display.platform).append('\n');

        builder.append("Graphics:\t").append(display.provider).append('\n');
        builder.append("GLVendor:\t").append(display.glVendor).append('\n');
        builder.append("GLRenderer:\t").append(display.glRenderer).append('\n');
        builder.append("GLVersion:\t").append(display.glVersion).append('\n');
        builder.append("GLSLVersion:\t").append(display.glslVersion).append('\n');
        builder.append("GLExtensions:\t").append(display.extensions).append('\n');

        builder.append("Handler:\t").append(getClass().getCanonicalName()).append('\n');

        return builder.toString();
    }

    void dumpState() {
        System.out.println(toString());
    }

    public void skipRender() {
        skipRender = true;
    }
}
