/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.provider;

import co.cata.ecs.engine.GameObject;
import co.cata.ecs.renderer.Shader;
import java.net.URL;
import org.apache.log4j.LogManager;
import org.lwjgl.opengl.GL11;

public class LWJGL3RendererGL15 extends LWJGL3Renderer {

    public LWJGL3RendererGL15(LWJGL3Display display) {
        super(display);
        logger = LogManager.getLogger(LWJGL3RendererGL2.class);
    }

    @Override
    public void init() {
        GL11.glClearColor(backgroundColor.getRed() / 255F, backgroundColor.getGreen() / 255F, backgroundColor.getBlue() / 255F, 1.0f);

    }

    @Override
    public Shader createShader(String vertexSource, String fragmentSource) {
        throw new UnsupportedOperationException("Not supported on 1.5");
    }

    public Shader createShader(URL vertexURL, URL fragmentURL) {
        throw new UnsupportedOperationException("Not supported on 1.5");
    }

    @Override
    public void releaseShader(Shader shader) {
        throw new UnsupportedOperationException("Not supported on 1.5");
    }

    @Override
    public Shader createDefaultShader(String vertexSource, String fragmentSource) {
        throw new UnsupportedOperationException("Not supported on 1.5");
    }
    
    @Override
    public Shader createDefaultShader(URL vertexURL, URL fragmentURL) {
        throw new UnsupportedOperationException("Not supported on 1.5");
    }

    @Override
    public Shader getDefaultShader() {
        throw new UnsupportedOperationException("Not supported on 1.5");
    }

    @Override
    public void completeRender() {
        synchronized (this) {
            if (skipRender) {
                batch.clear();
                lock = false;
                return;
            }

            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

            for (GameObject gameObject : batch.queue) {

                if (gameObject.geometry == null || !gameObject.geometry.isValid()) {
                    continue;
                }
                if (gameObject.spatial == null) {
                    continue;
                }

            }
        }
    }


}
