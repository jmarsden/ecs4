/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.provider;

import co.cata.ecs.renderer.Shader;
import co.cata.ecs.renderer.ShaderAttribute;
import co.cata.ecs.renderer.Uniform;
import co.cata.math.Matrix4f;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import org.lwjgl.opengl.GL20;
import static org.lwjgl.opengl.GL20.GL_ACTIVE_ATTRIBUTES;
import static org.lwjgl.opengl.GL20.GL_ACTIVE_UNIFORMS;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDeleteProgram;
import static org.lwjgl.opengl.GL20.glDeleteShader;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniformMatrix2fv;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUseProgram;

public class StringLWJGLShader extends Shader {

    private static final int NO_SHADER = 0;
    public boolean inError;
    public String vertexCompileError;
    public String fragmentCompileError;
    public String linkError;
    public boolean compiled;
    public ShaderAttribute[] attributes;
    public Uniform[] uniforms;
    /**
     * TODO: Make this better (if its possible).
     */
    FloatBuffer matrix44Buffer = BufferUtils.createFloatBuffer(16);

    public StringLWJGLShader(String vertexShaderSource, String fragmentShaderSource) {
        this.vertexShaderSource = vertexShaderSource;
        this.fragmentShaderSource = fragmentShaderSource;

        this.compiled = false;
        this.uniforms = null;

        inError = false;
    }

    public StringLWJGLShader(URL vertexURL, URL fragmentURL) {

        StringBuilder vertexSource = new StringBuilder();
        StringBuilder fragmentSource = new StringBuilder();
        try {
            InputStreamReader streamReader = new InputStreamReader(vertexURL.openStream());
            BufferedReader reader = new BufferedReader(streamReader);
            String line;
            while ((line = reader.readLine()) != null) {
                vertexSource.append(line).append("\n");
            }
            reader.close();

            streamReader = new InputStreamReader(fragmentURL.openStream());
            reader = new BufferedReader(streamReader);

            while ((line = reader.readLine()) != null) {
                fragmentSource.append(line).append("\n");
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Could not read file.");
        }

        this.vertexShaderSource = vertexSource.toString();
        this.fragmentShaderSource = fragmentSource.toString();

        this.compiled = false;
        this.uniforms = null;

        inError = false;

    }

    @Override
    public boolean inError() {
        return inError;
    }

    @Override
    public String getVertexCompileError() {
        return vertexCompileError;
    }

    @Override
    public String getFragmentCompileError() {
        return fragmentCompileError;
    }

    @Override
    public String getLinkError() {
        return linkError;
    }

    @Override
    public boolean isInitialized() {
        return vertexShaderSource != null && fragmentShaderSource != null;
    }

    @Override
    public void init() {
    }

    @Override
    public boolean isLoaded() {
        return compiled;
    }

    @Override
    public void load() {
        if (compiled) {
            return;
        }

        vertexShaderHandle = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShaderHandle, vertexShaderSource);
        glCompileShader(vertexShaderHandle);

        fragmentShaderHandle = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShaderHandle, fragmentShaderSource);
        glCompileShader(fragmentShaderHandle);

        int status = glGetShaderi(vertexShaderHandle, GL_COMPILE_STATUS);
        if (status != GL_TRUE) {
            throw new RuntimeException(glGetShaderInfoLog(vertexShaderHandle));
        }

        status = glGetShaderi(fragmentShaderHandle, GL_COMPILE_STATUS);
        if (status != GL_TRUE) {
            throw new RuntimeException(glGetShaderInfoLog(fragmentShaderHandle));
        }

        programHandle = glCreateProgram();
        glAttachShader(programHandle, vertexShaderHandle);
        glAttachShader(programHandle, fragmentShaderHandle);
        
//glBindFragDataLocation(shaderProgram, 0, "fragColor");

        
        glLinkProgram(programHandle);

        status = glGetProgrami(programHandle, GL_LINK_STATUS);
        if (status != GL_TRUE) {
            throw new RuntimeException(glGetProgramInfoLog(programHandle));
        }

        glUseProgram(programHandle);
        
        compiled = true;
    }

    @Override
    public void unLoad() {
        if (programHandle != -1) {
            glDeleteProgram(programHandle);
            programHandle = -1;
        }
        if (fragmentShaderHandle != -1) {
            glDeleteShader(fragmentShaderHandle);
            fragmentShaderHandle = -1;
        }
        if (vertexShaderHandle != -1) {
            glDeleteShader(vertexShaderHandle);
            vertexShaderHandle = -1;
        }
        attributes = null;
        uniforms = null;
        compiled = false;
    }

    @Override
    public void release() {
        this.vertexShaderSource = null;
        this.fragmentShaderSource = null;
    }

    @Override
    public Uniform[] getUniforms() {
        if (uniforms != null) {
            return uniforms;
        }
        IntBuffer size = BufferUtils.createIntBuffer(2);
        IntBuffer type = BufferUtils.createIntBuffer(2);
        
        int numUniforms = glGetProgrami(programHandle, GL_ACTIVE_UNIFORMS);
        uniforms = new LWJGL3Uniform[numUniforms];
        for (int i = NO_SHADER; i < numUniforms; i++) {
            size.clear();
            type.clear();
            String name = GL20.glGetActiveUniform(programHandle, i, size, type);
            int location = GL20.glGetUniformLocation(programHandle, name);
            
            int typeValue = type.get(NO_SHADER);
            Uniform uniform = new LWJGL3Uniform();
            uniform.name = name;
            uniform.location = location;
            uniform.type = typeValue;
            
            uniforms[i] = uniform;
        }
        return uniforms;
    }

    @Override
    public ShaderAttribute[] getAttributes() {
        if (attributes != null) {
            return attributes;
        }
        IntBuffer size = BufferUtils.createIntBuffer(2);
        IntBuffer type = BufferUtils.createIntBuffer(2);

        int numAttributes = glGetProgrami(programHandle, GL_ACTIVE_ATTRIBUTES);

        attributes = new ShaderAttribute[numAttributes];

        for (int i = NO_SHADER; i < numAttributes; i++) {
             size.clear();
            type.clear();
            String name = GL20.glGetActiveAttrib(programHandle, i, size, type);
            int location = GL20.glGetAttribLocation(programHandle, name);

            int typeValue = type.get(NO_SHADER);
            ShaderAttribute attribute = new LWJGL3ShaderAttribute();
            attribute.name = name;
            attribute.location = location;
            attribute.type = typeValue;

            attributes[i] = attribute;
        }
        return attributes;
    }

    @Override
    public boolean hasAttribute(String name) {
        if (attributes == null) {
            getAttributes();
        }
        for (ShaderAttribute attribute : attributes) {
            if (attribute.name != null && attribute.name.equals(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasAttribute(int location) {
        if (attributes == null) {
            getAttributes();
        }
        for (ShaderAttribute attribute : attributes) {
            if (attribute.location == location) {
                return true;
            }
        }
        return false;
    }

    @Override
    public ShaderAttribute getAttribute(String name) {
        if (attributes == null) {
            getAttributes();
        }
        for (ShaderAttribute attribute : attributes) {
            if (attribute.name != null && attribute.name.equals(name)) {
                return attribute;
            }
        }
        return null;
    }

    @Override
    public int getAttributeLocation(String name) {
        if (attributes == null) {
            getAttributes();
        }
        for (ShaderAttribute attribute : attributes) {
            if (attribute.name != null && attribute.name.equals(name)) {
                return attribute.location;
            }
        }
        return -1;
    }

    @Override
    public ShaderAttribute getAttribute(int location) {
        if (attributes == null) {
            getAttributes();
        }
        for (ShaderAttribute attribute : attributes) {
            if (attribute.location == location) {
                return attribute;
            }
        }
        return null;
    }

    @Override
    public boolean hasUniform(String name) {
        for (Uniform uniform : getUniforms()) {
            if (uniform.name != null && uniform.name.equals(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasUniform(int location) {
        for (Uniform uniform : getUniforms()) {
            if (uniform.location == location) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Uniform getUniform(String name) {
        for (Uniform uniform : getUniforms()) {
            if (uniform.name != null && uniform.name.equals(name)) {
                return uniform;
            }
        }
        return null;
    }

    @Override
    public Uniform getUniform(int location) {
        for (Uniform uniform : getUniforms()) {
            if (uniform.location == location) {
                return uniform;
            }
        }
        return null;
    }

    @Override
    public int getUniformLocation(String name) {
        for (Uniform uniform : getUniforms()) {
            if (uniform.name != null && uniform.name.equals(name)) {
                return uniform.location;
            }
        }
        return -1;
    }

    @Override
    public void enable() {
        if (!isInitialized()) {
            init();
        }
        if (!isLoaded()) {
            load();
        }
        GL20.glUseProgram(programHandle);
    }

    @Override
    public void disable() {
        GL20.glUseProgram(NO_SHADER);
    }

    /**
     * Store a uniform in the Shader. This is not thread-safe.
     *
     * @param name the name of the uniform. If you know the id then use the
     * overload..
     * @param matrix the matrix that will be set.
     */
    @Override
    public void loadUniform(String name, Matrix4f matrix) {
        if (!hasUniform(name)) {
            return;
        }
        int location = getUniformLocation(name);
        loadUniform(location, matrix);
    }

    /**
     * Store a uniform in the Shader. This is not thread-safe.
     *
     * @param location the location for the uniform within the compiled shader.
     * @param matrix the matrix that will be set.
     */
    @Override
    public void loadUniform(int location, Matrix4f matrix) {
        if (!hasUniform(location)) {
            return;
        }
        matrix.loadColumnMajor(matrix44Buffer);
        matrix44Buffer.flip();
        //GL20.glUniformMatrix4(location, false, matrix44Buffer);
        glUniformMatrix4fv(location, false, matrix44Buffer);
    }

}
