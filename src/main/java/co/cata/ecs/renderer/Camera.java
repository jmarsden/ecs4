/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.renderer;

import co.cata.math.Matrix3f;
import co.cata.math.Matrix4f;
import co.cata.math.Vector3f;

public abstract class Camera {

    protected boolean dirty;
    public Vector3f translation;
    public Vector3f scale;
    public Matrix3f rotation;
    public float[] mat4x4;
    public Matrix4f storage;
    

    public Camera() {
        translation = new Vector3f();
        scale = new Vector3f(1f, 1f, 1f);
        rotation = Matrix3f.identity();

        storage = Matrix4f.identity();
        
        mat4x4 = new float[16];

        load(mat4x4);
        dirty = false;
    }

    public synchronized void setTranslation(Vector3f translation) {
        this.translation.setData(translation);
        dirty = true;
    }

    public synchronized void setRotation(Matrix3f rotation) {
        this.rotation.setData(rotation);
        dirty = true;
    }

    public synchronized void setScale(Matrix3f scale) {
        this.rotation.setData(scale);
        dirty = true;
    }

    public float[] load(float[] mat4x4) {

        if (mat4x4 == null || mat4x4.length != 16) {
            throw new NullPointerException("Need a better error here");
        }

        mat4x4[0] = 1;
        mat4x4[1] = 0;
        mat4x4[2] = 0;
        mat4x4[3] = 0;
        mat4x4[4] = 0;
        mat4x4[5] = 1;
        mat4x4[6] = 0;
        mat4x4[7] = 0;
        mat4x4[8] = 0;
        mat4x4[9] = 0;
        mat4x4[10] = 1;
        mat4x4[11] = 0;
        mat4x4[12] = 0;
        mat4x4[13] = 0;
        mat4x4[14] = 0;
        mat4x4[15] = 1;

        return mat4x4;
    }

    public synchronized float[] get() {
        if (dirty) {
            load(mat4x4);
            dirty = false;
        }
        return mat4x4;
    }

    public void load(Matrix4f matrix) {
        matrix.mat4x4[0] = 1;
        matrix.mat4x4[1] = 0;
        matrix.mat4x4[2] = 0;
        matrix.mat4x4[3] = 0;
        matrix.mat4x4[4] = 0;
        matrix.mat4x4[5] = 1;
        matrix.mat4x4[6] = 0;
        matrix.mat4x4[7] = 0;
        matrix.mat4x4[8] = 0;
        matrix.mat4x4[9] = 0;
        matrix.mat4x4[10] = 1;
        matrix.mat4x4[11] = 0;
        matrix.mat4x4[12] = 0;
        matrix.mat4x4[13] = 0;
        matrix.mat4x4[14] = 0;
        matrix.mat4x4[15] = 1f;
    }
}
