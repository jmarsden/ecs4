/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.renderer;

/**
 *
 * @author jmarsden
 */
public class LookAt {
    
    float cameraX;
    float cameraY;
    float cameraZ;
    float focusX;
    float focusY;
    float focusZ;
    float upX;
    float upY;
    float upZ;
    
    public LookAt(float cameraX, float cameraY, float cameraZ,
            float focusX, float focusY, float focusZ,
            float upX, float upY, float upZ) {
        this.cameraX = cameraX;
        this.cameraY = cameraY;
        this.cameraZ = cameraZ;
        this.focusX = focusX;
        this.focusY = focusY;
        this.focusZ = focusZ;
        this.upX = upX;
        this.upY = upY;
        this.upZ = upZ;
    }

    public void get(float[] view44Matrix) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
