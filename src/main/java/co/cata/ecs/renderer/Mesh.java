/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.renderer;

public abstract class Mesh implements Lifecycle {

    public abstract void enable();
    
    public abstract void enable(Shader shader);
    
    public abstract void disable();
    
    public abstract void disable(Shader shader);
    
    public String dumpState() {
        StringBuilder state = new StringBuilder();
        state.append(getClass().getCanonicalName()).append('\n');
        state.append("isIntialised[").append(isInitialized()).append("] ").append("isLoaded[").append(isLoaded()).append("]");;
        return state.toString();
    }

    public abstract int getIndicesCount();
}
