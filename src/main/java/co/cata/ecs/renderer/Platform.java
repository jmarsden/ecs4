/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.renderer;

public class Platform {

    public static final int LINUX = 0;
    public static final String LINUX_NAME = "GNU\\Linux";
    public static final int OSX = 1;
    public static final String OSX_NAME = "Apple OSX";
    public static final int WINDOWS = 2;
    public static final String WINDOWS_NAME = "Microsoft Windows";
    public static final int UNKOWN = 3;
    public static final String UNKOWN_NAME = "Unknown";
    public final int platform;
    public final String platformName;

    public Platform(int platform, String platformName) {
        this.platform = platform;
        this.platformName = platformName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + this.platform;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Platform other = (Platform) obj;
        if (this.platform != other.platform) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return platformName;
    }
}
