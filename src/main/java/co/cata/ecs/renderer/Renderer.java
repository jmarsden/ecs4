/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.renderer;

import co.cata.ecs.component.spatial.SpatialComponent;
import co.cata.ecs.engine.GameObject;
import co.cata.graphics.Vertex;
import java.net.URL;
import java.nio.ByteBuffer;

public interface Renderer {
    
        
    /**
     * Management 
     */
    
    public void setIcon(ByteBuffer[] icons);
    
    public void setOrthoProjection(float left, float right, float top, float bottom);
    
    public void setOrthoProjection(float left, float right, float top, float bottom, float near, float far);
    
    public void setPerspectiveProjection(float near, float far, float fov, float aspectRatio);
    
    /**
     * Accessors
     */
    public GLVersion getGLVersion();
    
    public GLSLVersion getGLSLVersion();
    
    public GLVendor getGLVendor();
    
    public Platform getPlatform();
    
    public Provider getProvider();
    
    public Extensions getExtensions();
    
    public Projection getProjection();

    public Display getDisplay();
            
    public Camera getCamera();

    /**
     * Resources 
     */
    public Shader createShader(String vertexSource, String fragmentSource);
    
    public Shader createShader(URL vertexURL, URL fragmentURL);
    
    public void releaseShader(Shader shader);
    
    public Shader createDefaultShader(String vertexSource, String fragmentSource);
    
    public Shader createDefaultShader(URL vertexURL, URL fragmentURL);
    
    public Shader getDefaultShader();
    
    public Texture createTexture(String path);
    
    public void releaseTexture(Texture texture);
    
    public Mesh createStaticMesh(Vertex[] data, short[] indicies);
    
    public Mesh createStaticMesh(Shader shader, Vertex[] vertices, short[] indicies);
    
    public void releaseMesh(Mesh mesh);
    
    /**
     * Rendering 
     */
    public void setFrameRate(int frameRate);
    
    public void setVSyncEnabled(boolean vSync);
    
    public void startRender();

    public void addToRenderQueue(GameObject object);

    public boolean isRendering();

    public void completeRender();
    
    public boolean cull(SpatialComponent spatial);
    
    public void setRenderState(RenderState renderState);
    
    public void skipRender();

}
