/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.renderer;

public class ShaderDefaults {

    public static final String DEFAULT_VERTEX_SHADER_120 = "#version 120\r\n"
            + "\r\n"
            + "attribute vec4 in_Position;\n"
            + "attribute vec4 in_Color;\r\n"
            + "attribute vec2 in_TextureCoord;\r\n"
            + "\r\n"
            + "uniform mat4 projectionMatrix;\r\n"
            + "uniform mat4 viewMatrix;\r\n"
            + "uniform mat4 modelMatrix;\r\n"
            + "\r\n"
            + "varying vec4 pass_Color;\r\n"
            + "varying vec2 pass_TextureCoord;\r\n"
            + "\r\n"
            + "void main() {\r\n"
            + " pass_Color = in_Color;\r\n"
            + " pass_TextureCoord = in_TextureCoord;\r\n"
            + " gl_Position = projectionMatrix * viewMatrix * modelMatrix * in_Position;\r\n"
            + "}";
    public static final String DEFAULT_FRAGMENT_SHADER_120 = "#version 120\r\n"
            + "\r\n"
            + "varying vec4 pass_Color;\r\n"
            + "varying vec2 pass_TextureCoord;\r\n"
            + "\r\n"
            + "uniform sampler2D textureDiffuse;\r\n"
            + "void main() {\r\n"
            + " gl_FragColor = pass_Color * texture2D(textureDiffuse, pass_TextureCoord);\r\n"
            + "}";
    public static final String DEFAULT_VERTEX_SHADER_130 = "#version 130\r\n"
            + "\r\n"
            + "uniform mat4 projectionMatrix;\r\n"
            + "uniform mat4 viewMatrix;\r\n"
            + "uniform mat4 modelMatrix;\r\n"
            + "\r\n"
            + "in vec4 in_Position;\r\n"
            + "in vec4 in_Color;\r\n"
            + "in vec2 in_TextureCoord;\r\n"
            + "\r\n"
            + "out vec4 pass_Color;\r\n"
            + "out vec2 pass_TextureCoord;\r\n"
            + "\r\n"
            + "void main() {\r\n"
            + "	gl_Position = projectionMatrix * viewMatrix * modelMatrix * in_Position;\r\n"
            + "	\r\n"
            + "	pass_Color = in_Color;\r\n"
            + "	pass_TextureCoord = in_TextureCoord;\r\n"
            + "}";
    
    public static final String DEFAULT_FRAGMENT_SHADER_130 = "#version 130\r\n"
            + "\r\n"
            + "uniform sampler2D textureDiffuse;\r\n"
            + "\r\n"
            + "in vec4 pass_Color;\r\n"
            + "in vec2 pass_TextureCoord;\r\n"
            + "\r\n"
            + "out vec4 out_Color;\r\n"
            + "\r\n"
            + "void main() {\r\n"
            + "	out_Color = pass_Color * texture2D(textureDiffuse, pass_TextureCoord);\r\n"
            + "}";
    
    
    public static final String DEFAULT_FRAGMENT_SHADER_140 = "#version 140\r\n"
            + "\r\n"
            + "uniform sampler2D textureDiffuse;\r\n"
            + "\r\n"
            + "in vec4 pass_Color;\r\n"
            + "in vec2 pass_TextureCoord;\r\n"
            + "\r\n"
            + "out vec4 out_Color;\r\n"
            + "\r\n"
            + "void main() {\r\n"
            + "	out_Color = pass_Color * texture2D(textureDiffuse, pass_TextureCoord);\r\n"
            + "}";
    
    public static final String DEFAULT_VERTEX_SHADER_150 = "#version 150\r\n"
            + "\r\n"
            + "uniform mat4 projectionMatrix;\r\n"
            + "uniform mat4 viewMatrix;\r\n"
            + "uniform mat4 modelMatrix;\r\n"
            + "\r\n"
            + "in vec4 in_Position;\r\n"
            + "in vec4 in_Color;\r\n"
            + "in vec2 in_TextureCoord;\r\n"
            + "\r\n"
            + "out vec4 pass_Color;\r\n"
            + "out vec2 pass_TextureCoord;\r\n"
            + "\r\n"
            + "void main() {\r\n"
            + "	gl_Position = projectionMatrix * viewMatrix * modelMatrix * in_Position;\r\n"
            + "	\r\n"
            + "	pass_Color = in_Color;\r\n"
            + "	pass_TextureCoord = in_TextureCoord;\r\n"
            + "}";
    
    
    public static final String DEFAULT_FRAGMENT_SHADER_150 = "#version 150\r\n"
            + "\r\n"
            + "uniform sampler2D textureDiffuse;\r\n"
            + "\r\n"
            + "in vec4 pass_Color;\r\n"
            + "in vec2 pass_TextureCoord;\r\n"
            + "\r\n"
            + "out vec4 out_Color;\r\n"
            + "\r\n"
            + "void main() {\r\n"
            + "	out_Color = pass_Color * texture2D(textureDiffuse, pass_TextureCoord);\r\n"
            + "}";
    
    public static final String DEFAULT_VERTEX_SHADER_140 = "#version 140\r\n"
            + "\r\n"
            + "uniform mat4 projectionMatrix;\r\n"
            + "uniform mat4 viewMatrix;\r\n"
            + "uniform mat4 modelMatrix;\r\n"
            + "\r\n"
            + "in vec4 in_Position;\r\n"
            + "in vec4 in_Color;\r\n"
            + "in vec2 in_TextureCoord;\r\n"
            + "\r\n"
            + "out vec4 pass_Color;\r\n"
            + "out vec2 pass_TextureCoord;\r\n"
            + "\r\n"
            + "void main() {\r\n"
            + "	gl_Position = projectionMatrix * viewMatrix * modelMatrix * in_Position;\r\n"
            + "	\r\n"
            + "	pass_Color = in_Color;\r\n"
            + "	pass_TextureCoord = in_TextureCoord;\r\n"
            + "}";
}
