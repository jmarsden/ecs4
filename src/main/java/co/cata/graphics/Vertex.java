/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.graphics;

import java.nio.FloatBuffer;

public class Vertex {

    private float[] xyzw;
    private float[] nxnynz;
    private float[] rgba;
    private float[] st;
    // Static variables
    public static final int POSITION;
    public static final int NORMAL;
    public static final int COLOR;
    public static final int TEXTURE;
    // The amount of bytes an element has
    public static final int ELEMENT_FLOAT_BYTES;
    // Elements per parameter
    public static final int POSITION_ELEMENT_COUNT;
    public static final int NORMAL_ELEMENT_COUNT;
    public static final int COLOR_ELEMENT_COUNT;
    public static final int TEXTURE_ELEMENT_COUNT;
    // Bytes per parameter
    public static final int POSITION_BYTE_COUNT;
    public static final int NORMAL_BYTE_COUNT;
    public static final int COLOR_BYTE_COUNT;
    public static final int TEXTURE_BYTE_COUNT;
    // Byte offsets per parameter
    public static final int POSITION_BYTE_OFFSET;
    public static final int NORMAL_BYTE_OFFSET;
    public static final int COLOR_BYTE_OFFSET;
    public static final int TEXTURE_BYTE_OFFSET;
    // The amount of elements that a vertex has
    public static final int ELEMENT_COUNT;
    // The size of a vertex in bytes, like in C/C++: sizeof(Vertex)
    public static final int STRIDE;

    static {
        POSITION = 1;
        NORMAL = 1 << 1;
        COLOR = 1 << 2;
        TEXTURE = 1 << 3;

        ELEMENT_FLOAT_BYTES = 4;

        POSITION_ELEMENT_COUNT = 4;
        NORMAL_ELEMENT_COUNT = 3;
        COLOR_ELEMENT_COUNT = 4;
        TEXTURE_ELEMENT_COUNT = 2;

        POSITION_BYTE_COUNT = POSITION_ELEMENT_COUNT * ELEMENT_FLOAT_BYTES;
        NORMAL_BYTE_COUNT = NORMAL_ELEMENT_COUNT * ELEMENT_FLOAT_BYTES;
        COLOR_BYTE_COUNT = COLOR_ELEMENT_COUNT * ELEMENT_FLOAT_BYTES;
        TEXTURE_BYTE_COUNT = TEXTURE_ELEMENT_COUNT * ELEMENT_FLOAT_BYTES;

        POSITION_BYTE_OFFSET = 0;
        NORMAL_BYTE_OFFSET = POSITION_BYTE_OFFSET + POSITION_BYTE_COUNT;
        COLOR_BYTE_OFFSET = NORMAL_BYTE_OFFSET + NORMAL_BYTE_COUNT;
        TEXTURE_BYTE_OFFSET = COLOR_BYTE_OFFSET + COLOR_BYTE_COUNT;

        ELEMENT_COUNT = POSITION_ELEMENT_COUNT + NORMAL_ELEMENT_COUNT + COLOR_ELEMENT_COUNT + TEXTURE_ELEMENT_COUNT;
        STRIDE = POSITION_BYTE_COUNT + NORMAL_BYTE_COUNT + COLOR_BYTE_COUNT + TEXTURE_BYTE_COUNT;
    }

    public Vertex() {
        xyzw = new float[]{0f, 0f, 0f, 1f};
        nxnynz = new float[]{0f, 0f, 1f};
        rgba = new float[]{1f, 1f, 1f, 1f};
        st = new float[]{0f, 0f};
    }

    public Vertex(float... data) {
        if (data == null) {
            throw new NullPointerException("That no worky");
        }
        int l = data.length;
        xyzw = new float[]{0f, 0f, 0f, 1f};
        nxnynz = new float[]{0f, 0f, 1f};
        rgba = new float[]{1f, 1f, 1f, 1f};
        st = new float[]{0f, 0f};

        switch (l) {
            case 13:
                xyzw[0] = data[0];
                xyzw[1] = data[1];
                xyzw[2] = data[2];
                xyzw[3] = data[3];

                nxnynz[0] = data[4];
                nxnynz[1] = data[5];
                nxnynz[2] = data[6];

                rgba[0] = data[7];
                rgba[1] = data[8];
                rgba[2] = data[9];
                rgba[3] = data[10];

                st[0] = data[11];
                st[1] = data[12];
                break;
            case 11:
                xyzw[0] = data[0];
                xyzw[1] = data[1];
                xyzw[2] = data[2];
                xyzw[3] = data[3];

                nxnynz[0] = data[4];
                nxnynz[1] = data[5];
                nxnynz[2] = data[6];

                rgba[0] = data[7];
                rgba[1] = data[8];
                rgba[2] = data[9];
                rgba[3] = data[10];

                break;
            case 9:
                xyzw[0] = data[0];
                xyzw[1] = data[1];
                xyzw[2] = data[2];
                xyzw[3] = data[3];

                nxnynz[0] = data[4];
                nxnynz[1] = data[5];
                nxnynz[2] = data[6];

                st[0] = data[7];
                st[1] = data[8];
                break;

            case 8:
                xyzw[0] = data[0];
                xyzw[1] = data[1];
                xyzw[2] = data[2];
                xyzw[3] = data[3];

                rgba[0] = data[4];
                rgba[1] = data[5];
                rgba[2] = data[6];
                rgba[3] = data[7];
                break;

            case 6:
                xyzw[0] = data[0];
                xyzw[1] = data[1];
                xyzw[2] = data[2];
                xyzw[3] = data[3];

                st[0] = data[4];
                st[1] = data[5];
                break;

            default:
                throw new RuntimeException("Cannot init with a float that size. You should read the doco.");
        }
    }

    public void setXYZ(float x, float y, float z) {
        this.setXYZW(x, y, z, 1f);
    }

    public void setRGB(float r, float g, float b) {
        this.setRGBA(r, g, b, 1f);
    }

    public void setST(float s, float t) {
        this.st = new float[]{s, t};
    }

    public void setXYZW(float x, float y, float z, float w) {
        this.xyzw = new float[]{x, y, z, w};
    }

    public void setRGBA(float r, float g, float b, float a) {
        this.rgba = new float[]{r, g, b, 1f};
    }

    public void setN(float nx, float ny, float nz) {
        this.nxnynz = new float[]{nx, ny, nz};
    }

    public float[] getXYZW() {
        return new float[]{this.xyzw[0], this.xyzw[1], this.xyzw[2], this.xyzw[3]};
    }

    public float[] getXYZ() {
        return new float[]{this.xyzw[0], this.xyzw[1], this.xyzw[2]};
    }

    public float[] getRGBA() {
        return new float[]{this.rgba[0], this.rgba[1], this.rgba[2], this.rgba[3]};
    }

    public float[] getRGB() {
        return new float[]{this.rgba[0], this.rgba[1], this.rgba[2]};
    }

    public float[] getST() {
        return new float[]{this.st[0], this.st[1]};
    }

    public float[] getN() {
        return new float[]{this.nxnynz[0], this.nxnynz[1], this.nxnynz[2]};
    }

    // Getters	
    public float[] getElements() {
        float[] out = new float[Vertex.ELEMENT_COUNT];
        int i = 0;
        // Insert XYZW elements
        out[i++] = this.xyzw[0];
        out[i++] = this.xyzw[1];
        out[i++] = this.xyzw[2];
        out[i++] = this.xyzw[3];
        // Insert Normal
        out[i++] = this.nxnynz[0];
        out[i++] = this.nxnynz[1];
        out[i++] = this.nxnynz[2];
        // Insert RGBA elements
        out[i++] = this.rgba[0];
        out[i++] = this.rgba[1];
        out[i++] = this.rgba[2];
        out[i++] = this.rgba[3];
        // Insert ST elements
        out[i++] = this.st[0];
        out[i++] = this.st[1];
        return out;
    }

    public FloatBuffer getPosition(FloatBuffer buff) {
        buff.put(this.xyzw);
        return buff;
    }

    public FloatBuffer getNormal(FloatBuffer buff) {
        buff.put(this.nxnynz);
        return buff;
    }

    public FloatBuffer getColor(FloatBuffer buff) {
        buff.put(this.rgba);
        return buff;
    }

    public FloatBuffer getST(FloatBuffer buff) {
        buff.put(this.st);
        return buff;
    }

    public FloatBuffer get(int flag, FloatBuffer buff) {
        if ((flag & POSITION) == POSITION) {
            buff.put(this.xyzw);
        }
        if ((flag & NORMAL) == NORMAL) {
            buff.put(this.nxnynz);
        }
        if ((flag & COLOR) == COLOR) {
            buff.put(this.rgba);
        }
        if ((flag & TEXTURE) == TEXTURE) {
            buff.put(this.st);
        }
        return buff;
    }

    public static int getElementCount(int flag) {
        int size = 0;
        if ((flag & POSITION) == POSITION) {
            size += POSITION_ELEMENT_COUNT;
        }
        if ((flag & NORMAL) == NORMAL) {
            size += NORMAL_ELEMENT_COUNT;
        }
        if ((flag & COLOR) == COLOR) {
            size += COLOR_ELEMENT_COUNT;
        }
        if ((flag & TEXTURE) == TEXTURE) {
            size += TEXTURE_ELEMENT_COUNT;
        }
        return size;
    }

    public static int getByteCount(int flag) {
        int size = 0;
        if ((flag & POSITION) == POSITION) {
            size += POSITION_BYTE_COUNT;
        }
        if ((flag & NORMAL) == NORMAL) {
            size += NORMAL_BYTE_COUNT;
        }
        if ((flag & COLOR) == COLOR) {
            size += COLOR_BYTE_COUNT;
        }
        if ((flag & TEXTURE) == TEXTURE) {
            size += TEXTURE_BYTE_COUNT;
        }
        return size;
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        output.append("[xyzw:<").append(xyzw[0]).append(',').append('\t').append(xyzw[1]).append(',').append('\t').append(xyzw[2]).append(',').append('\t').append(xyzw[3]).append(">]");
        output.append("[nxnynz:<").append(nxnynz[0]).append(',').append('\t').append(nxnynz[1]).append(',').append('\t').append(nxnynz[2]).append(',').append(">]");
        output.append("[rgba:<").append(rgba[0]).append(',').append('\t').append(rgba[1]).append(',').append('\t').append(rgba[2]).append(',').append('\t').append(rgba[3]).append(">]");
        output.append("[st:<").append(st[0]).append(',').append('\t').append(st[1]).append(">]");
        return output.toString();
    }
}