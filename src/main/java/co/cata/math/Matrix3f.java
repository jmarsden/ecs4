/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.math;

/**
 * Performance optimised 3 dimensional float matrix. Data storage is row major. 
 *
 * @author j.w.marsden@gmail.com
 */
public class Matrix3f extends MatrixF {

    float m00, m01, m02, m10, m11, m12, m20, m21, m22 = 0F;

    public Matrix3f(float m00, float m01, float m02, float m10, float m11,
            float m12, float m20, float m21, float m22) {
        super(3, 3);
        this.m00 = m00;
        this.m01 = m01;
        this.m02 = m02;
        this.m10 = m10;
        this.m11 = m11;
        this.m12 = m12;
        this.m20 = m20;
        this.m21 = m21;
        this.m22 = m22;
    }

    public Matrix3f(Float[][] data) {
        super(data);
        m = data.length;
        if(m > 0) {
            n = data[0].length;
        } else {
            throw new IllegalArgumentException("data dimensions must be > 0");
        }
        if(m != 3 || n != 3) {
            throw new IllegalArgumentException("data dimensions must be = 3");
        }
        m00 = data[0][0];
        m01 = data[0][1];
        m02 = data[0][2];
        m10 = data[1][0];
        m11 = data[1][1];
        m12 = data[1][2];
        m20 = data[2][0];
        m21 = data[2][1];
        m22 = data[2][2];
    }

    public Matrix3f() {
        super(3, 3);
    }

    @Override
    public Float[][] getData() {
        return new Float[][]{{m00, m01, m02}, {m10, m11, m12},
                    {m20, m21, m22}};
    }

    @Override
    public Float getData(int i, int j) {
        if(i > 2 || j > 2) {
            throw new IllegalArgumentException("i and j must be < 2");
        }
        return getData()[i][j];
    }

    @Override
    public void setData(Number[][] data) {
        m = data.length;
        if(m > 0) {
            n = data[0].length;
        } else {
            throw new IllegalArgumentException("data dimensions must be > 0");
        }
        if(m != 2 || n != 2) {
            throw new IllegalArgumentException("data dimensions must be = 3");
        }
        m00 = data[0][0].floatValue();
        m01 = data[0][1].floatValue();
        m02 = data[0][2].floatValue();
        m10 = data[1][0].floatValue();
        m11 = data[1][1].floatValue();
        m12 = data[1][2].floatValue();
        m20 = data[2][0].floatValue();
        m21 = data[2][1].floatValue();
        m22 = data[2][2].floatValue();
    }

    public void setData(Matrix3f data) {
        m00 = data.m00;
        m01 = data.m01;
        m02 = data.m02;
        m10 = data.m10;
        m11 = data.m11;
        m12 = data.m12;
        m20 = data.m20;
        m21 = data.m21;
        m22 = data.m22;
    }

    @Override
    public void setData(int i, int j, Number data) {
        if(i > 2 || j > 2) {
            throw new IllegalArgumentException("i and j must be <= 2");
        }
        if(i == 0 && j == 0) {
            m00 = data.floatValue();
        } else if(i == 0 && j == 1) {
            m01 = data.floatValue();
        } else if(i == 0 && j == 2) {
            m02 = data.floatValue();
        } else if(i == 1 && j == 0) {
            m10 = data.floatValue();
        } else if(i == 1 && j == 1) {
            m11 = data.floatValue();
        } else if(i == 1 && j == 2) {
            m12 = data.floatValue();
        } else if(i == 2 && j == 0) {
            m20 = data.floatValue();
        } else if(i == 2 && j == 1) {
            m21 = data.floatValue();
        } else if(i == 2 && j == 2) {
            m22 = data.floatValue();
        }
    }

    @Override
    public Float[] getRow(int i) {
        if(i > 2) {
            throw new IllegalArgumentException("i must be <= 2");
        }
        return getData()[i];
    }

    @Override
    public final Matrix3f add(Matrix b) {
        if(m != b.m || n != b.n) {
            throw new RuntimeException("Matrix dimensions are not equal.");
        }
        m00 += b.getData(0, 0).floatValue();
        m01 += b.getData(0, 1).floatValue();
        m02 += b.getData(0, 2).floatValue();
        m10 += b.getData(1, 0).floatValue();
        m11 += b.getData(1, 1).floatValue();
        m12 += b.getData(1, 2).floatValue();
        m20 += b.getData(2, 0).floatValue();
        m21 += b.getData(2, 1).floatValue();
        m22 += b.getData(2, 2).floatValue();
        return this;
    }

    @Override
    public MatrixF add(Matrix b, Matrix c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public final Matrix3f add3f(Matrix3f b) {
        if(m != b.m || n != b.n) {
            throw new RuntimeException("Matrix dimensions are not equal.");
        }
        m00 += b.m00;
        m01 += b.m01;
        m02 += b.m02;
        m10 += b.m10;
        m11 += b.m11;
        m12 += b.m12;
        m20 += b.m20;
        m21 += b.m21;
        m22 += b.m22;
        return this;
    }

    @Override
    public MatrixF subtract(Matrix b) {
        if(m != b.m || n != b.n) {
            throw new RuntimeException("Matrix dimensions are not equal.");
        }
        m00 -= b.getData(0, 0).floatValue();
        m01 -= b.getData(0, 1).floatValue();
        m02 -= b.getData(0, 2).floatValue();
        m10 -= b.getData(1, 0).floatValue();
        m11 -= b.getData(1, 1).floatValue();
        m12 -= b.getData(1, 2).floatValue();
        m20 -= b.getData(2, 0).floatValue();
        m21 -= b.getData(2, 1).floatValue();
        m22 -= b.getData(2, 2).floatValue();
        return this;
    }

    public final Matrix3f subtract3f(Matrix3f b) {
        if(m != b.m || n != b.n) {
            throw new RuntimeException("Matrix dimensions are not equal.");
        }
        m00 -= b.m00;
        m01 -= b.m01;
        m02 -= b.m02;
        m10 -= b.m10;
        m11 -= b.m11;
        m12 -= b.m12;
        m20 -= b.m20;
        m21 -= b.m21;
        m22 -= b.m22;
        return this;
    }

    @Override
    public MatrixF subtract(Matrix b, Matrix c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public MatrixF multiply(Matrix b) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF multiply(Matrix b, Matrix c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Matrix3f multiply3f(Matrix3f b) {
        float c00 = m00 * b.m00 + m10 * b.m01 + m20 * b.m02;
        float c01 = m01 * b.m00 + m11 * b.m01 + m21 * b.m02;
        float c02 = m02 * b.m00 + m12 * b.m01 + m22 * b.m02;

        float c10 = m00 * b.m10 + m10 * b.m11 + m20 * b.m12;
        float c11 = m01 * b.m10 + m11 * b.m11 + m21 * b.m12;
        float c12 = m02 * b.m10 + m12 * b.m11 + m22 * b.m12;

        float c20 = m00 * b.m20 + m10 * b.m21 + m20 * b.m22;
        float c21 = m01 * b.m20 + m11 * b.m21 + m21 * b.m22;
        float c22 = m02 * b.m20 + m12 * b.m21 + m22 * b.m22;

        m00 = c00;
        m01 = c01;
        m02 = c02;
        m10 = c10;
        m11 = c11;
        m12 = c12;
        m20 = c20;
        m21 = c21;
        m22 = c22;

        return this;
    }

    public Matrix3f multiply3f(Matrix3f b, Matrix3f c) {
        c.m00 = m00 * b.m00 + m10 * b.m01 + m20 * b.m02;
        c.m01 = m01 * b.m00 + m11 * b.m01 + m21 * b.m02;
        c.m02 = m02 * b.m00 + m12 * b.m01 + m22 * b.m02;

        c.m10 = m00 * b.m10 + m10 * b.m11 + m20 * b.m12;
        c.m11 = m01 * b.m10 + m11 * b.m11 + m21 * b.m12;
        c.m12 = m02 * b.m10 + m12 * b.m11 + m22 * b.m12;

        c.m20 = m00 * b.m20 + m10 * b.m21 + m20 * b.m22;
        c.m21 = m01 * b.m20 + m11 * b.m21 + m21 * b.m22;
        c.m22 = m02 * b.m20 + m12 * b.m21 + m22 * b.m22;

        return c;
    }

    @Override
    public MatrixF transpose() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Matrix transpose(Matrix r) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF addScalar(Number v) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF addScalar(Number v, Matrix c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public MatrixF subtractScalar(Number v) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF subtractScalar(Number v, Matrix c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public MatrixF multiplyScalar(Number v) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF multiplyScalar(Number v, Matrix c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public MatrixF divideScalar(Number v) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF divideScalar(Number v, Matrix c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public MatrixF invert() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Matrix invert(Matrix r) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public VectorF transformVector(VectorF v) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public Vector2f transformVector2f(Vector2f v) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public Vector2f transformVector2f(Vector2f v, Vector2f r) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public Vector3f transformVector3f(Vector3f v) {
        return transformVector3f(v, v);
    }

    public Vector3f transformVector3f(Vector3f v, Vector3f r) {
        float x = m00 * v.x + m01 * v.y + m02 * v.z;
        float y = m10 * v.x + m11 * v.y + m12 * v.z;
        float z = m20 * v.x + m21 * v.y + m22 * v.z;
        r.x = x;
        r.y = y;
        r.z = z;
        return r;
    }

    public void setIdentity() {
        m00 = 1;
        m01 = 0;
        m02 = 0;
        m10 = 0;
        m11 = 1;
        m12 = 0;
        m20 = 0;
        m21 = 0;
        m22 = 1;
    }

    public static Matrix3f identity() {
        return new Matrix3f(1f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 1f);
    }
}