/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.math;

import java.nio.FloatBuffer;

/**
 * Performance optimised 4 dimensional float matrix. Data storage is row major. 
 *
 * @author j.w.marsden@gmail.com
 */
public class Matrix4f extends MatrixF {

    public float[] mat4x4;

    public Matrix4f(float m00, float m01, float m02, float m03,
            float m10, float m11, float m12, float m13,
            float m20, float m21, float m22, float m23,
            float m30, float m31, float m32, float m33) {
        super(4, 4);
        mat4x4 = new float[16];
        mat4x4[0] = m00;
        mat4x4[1] = m01;
        mat4x4[2] = m02;
        mat4x4[3] = m03;
        mat4x4[4] = m10;
        mat4x4[5] = m11;
        mat4x4[6] = m12;
        mat4x4[7] = m13;
        mat4x4[8] = m20;
        mat4x4[9] = m21;
        mat4x4[10] = m22;
        mat4x4[11] = m23;
        mat4x4[12] = m30;
        mat4x4[13] = m31;
        mat4x4[14] = m32;
        mat4x4[15] = m33;
    }

    public Matrix4f(float[] data) {
        super(4, 4);
        if (data == null || data.length != 16) {
            throw new NullPointerException("Data null or not correct length");
        }
        mat4x4 = new float[16];
        mat4x4[0] = data[0];
        mat4x4[1] = data[1];
        mat4x4[2] = data[2];
        mat4x4[3] = data[3];
        mat4x4[4] = data[4];
        mat4x4[5] = data[5];
        mat4x4[6] = data[6];
        mat4x4[7] = data[7];
        mat4x4[8] = data[8];
        mat4x4[9] = data[9];
        mat4x4[10] = data[10];
        mat4x4[11] = data[11];
        mat4x4[12] = data[12];
        mat4x4[13] = data[13];
        mat4x4[14] = data[14];
        mat4x4[15] = data[15];
    }

    public Matrix4f(Float[][] data) {
        m = data.length;
        if (m > 0) {
            n = data[0].length;
        } else {
            throw new IllegalArgumentException("data dimensions must be > 0");
        }
        if (m != 4 || n != 4) {
            throw new IllegalArgumentException("data dimensions must be = 3");
        }
        mat4x4 = new float[16];
        mat4x4[0] = data[0][0];
        mat4x4[1] = data[0][1];
        mat4x4[2] = data[0][2];
        mat4x4[3] = data[0][3];
        mat4x4[4] = data[1][0];
        mat4x4[5] = data[1][1];
        mat4x4[6] = data[1][2];
        mat4x4[7] = data[1][3];
        mat4x4[8] = data[2][0];
        mat4x4[9] = data[2][1];
        mat4x4[10] = data[2][2];
        mat4x4[11] = data[2][3];
        mat4x4[12] = data[3][0];
        mat4x4[13] = data[3][1];
        mat4x4[14] = data[3][2];
        mat4x4[15] = data[3][3];
    }

    public Matrix4f() {
        super(4, 4);
        mat4x4 = new float[16];
    }

    public Matrix4f(FloatBuffer values) {
        super(4, 4);
        if (values == null || values.capacity() != 16) {
            throw new IllegalArgumentException("data dimensions must 4x4");
        }
        mat4x4 = new float[16];
        values.rewind();
        while (values.hasRemaining()) {
            mat4x4[0] = values.get();
            mat4x4[1] = values.get();
            mat4x4[2] = values.get();
            mat4x4[3] = values.get();
            mat4x4[4] = values.get();
            mat4x4[5] = values.get();
            mat4x4[6] = values.get();
            mat4x4[7] = values.get();
            mat4x4[8] = values.get();
            mat4x4[9] = values.get();
            mat4x4[10] = values.get();
            mat4x4[11] = values.get();
            mat4x4[12] = values.get();
            mat4x4[13] = values.get();
            mat4x4[14] = values.get();
            mat4x4[15] = values.get();
        }
    }

    @Override
    public Float[][] getData() {
        return new Float[][]{
            {mat4x4[0], mat4x4[1], mat4x4[2], mat4x4[3]},
            {mat4x4[4], mat4x4[5], mat4x4[6], mat4x4[7]},
            {mat4x4[8], mat4x4[9], mat4x4[10], mat4x4[11]},
            {mat4x4[12], mat4x4[13], mat4x4[14], mat4x4[15]}
        };
    }

    @Override
    public Float getData(int i, int j) {
        switch (i) {
            case 0:
                switch (j) {
                    case 0:
                        return mat4x4[0];
                    case 1:
                        return mat4x4[1];
                    case 2:
                        return mat4x4[2];
                    case 3:
                        return mat4x4[3];
                    default:
                        throw new IllegalArgumentException("j must be < 4");
                }
            case 1:
                switch (j) {
                    case 0:
                        return mat4x4[4];
                    case 1:
                        return mat4x4[5];
                    case 2:
                        return mat4x4[6];
                    case 3:
                        return mat4x4[7];
                    default:
                        throw new IllegalArgumentException("j must be < 4");
                }
            case 2:
                switch (j) {
                    case 0:
                        return mat4x4[8];
                    case 1:
                        return mat4x4[9];
                    case 2:
                        return mat4x4[10];
                    case 3:
                        return mat4x4[11];
                    default:
                        throw new IllegalArgumentException("j must be < 4");
                }
            case 3:
                switch (j) {
                    case 0:
                        return mat4x4[12];
                    case 1:
                        return mat4x4[13];
                    case 2:
                        return mat4x4[14];
                    case 3:
                        return mat4x4[15];
                    default:
                        throw new IllegalArgumentException("j must be < 4");
                }
            default:
                throw new IllegalArgumentException("i must be < 4");
        }
    }

    @Override
    public void setData(Number[][] data) {
        m = data.length;
        if (m > 0) {
            n = data[0].length;
        } else {
            throw new IllegalArgumentException("data dimensions must be > 0");
        }
        if (m != 4 || n != 4) {
            throw new IllegalArgumentException("data dimensions must be = 3");
        }
        mat4x4[0] = data[0][0].floatValue();
        mat4x4[1] = data[0][1].floatValue();
        mat4x4[2] = data[0][2].floatValue();
        mat4x4[3] = data[0][3].floatValue();
        mat4x4[4] = data[1][0].floatValue();
        mat4x4[5] = data[1][1].floatValue();
        mat4x4[6] = data[1][2].floatValue();
        mat4x4[7] = data[1][3].floatValue();
        mat4x4[8] = data[2][0].floatValue();
        mat4x4[9] = data[2][1].floatValue();
        mat4x4[10] = data[2][2].floatValue();
        mat4x4[11] = data[2][3].floatValue();
        mat4x4[12] = data[3][0].floatValue();
        mat4x4[13] = data[3][1].floatValue();
        mat4x4[14] = data[3][2].floatValue();
        mat4x4[15] = data[3][3].floatValue();
    }

    public void setData(Matrix4f data) {
        mat4x4[0] = data.mat4x4[0];
        mat4x4[1] = data.mat4x4[1];
        mat4x4[2] = data.mat4x4[2];
        mat4x4[3] = data.mat4x4[3];
        mat4x4[4] = data.mat4x4[4];
        mat4x4[5] = data.mat4x4[5];
        mat4x4[6] = data.mat4x4[6];
        mat4x4[7] = data.mat4x4[7];
        mat4x4[8] = data.mat4x4[8];
        mat4x4[9] = data.mat4x4[9];
        mat4x4[10] = data.mat4x4[10];
        mat4x4[11] = data.mat4x4[11];
        mat4x4[12] = data.mat4x4[12];
        mat4x4[13] = data.mat4x4[13];
        mat4x4[14] = data.mat4x4[14];
        mat4x4[15] = data.mat4x4[15];
    }

    @Override
    public void setData(int i, int j, Number data) {
        switch (i) {
            case 0:
                switch (j) {
                    case 0:
                        mat4x4[0] = data.floatValue();
                        return;
                    case 1:
                        mat4x4[1] = data.floatValue();
                        return;
                    case 2:
                        mat4x4[2] = data.floatValue();
                        return;
                    case 3:
                        mat4x4[3] = data.floatValue();
                        return;
                    default:
                        throw new IllegalArgumentException("j must be < 4");
                }
            case 1:
                switch (j) {
                    case 0:
                        mat4x4[4] = data.floatValue();
                        return;
                    case 1:
                        mat4x4[5] = data.floatValue();
                        return;
                    case 2:
                        mat4x4[6] = data.floatValue();
                        return;
                    case 3:
                        mat4x4[7] = data.floatValue();
                        return;
                    default:
                        throw new IllegalArgumentException("j must be < 4");
                }
            case 2:
                switch (j) {
                    case 0:
                        mat4x4[8] = data.floatValue();
                        return;
                    case 1:
                        mat4x4[9] = data.floatValue();
                        return;
                    case 2:
                        mat4x4[10] = data.floatValue();
                        return;
                    case 3:
                        mat4x4[11] = data.floatValue();
                        return;
                    default:
                        throw new IllegalArgumentException("j must be < 4");
                }
            case 3:
                switch (j) {
                    case 0:
                        mat4x4[12] = data.floatValue();
                        return;
                    case 1:
                        mat4x4[13] = data.floatValue();
                        return;
                    case 2:
                        mat4x4[14] = data.floatValue();
                        return;
                    case 3:
                        mat4x4[15] = data.floatValue();
                        return;
                    default:
                        throw new IllegalArgumentException("j must be < 4");
                }
            default:
                throw new IllegalArgumentException("i must be < 4");
        }
    }

    public void setData(float[] data) {
        if (data == null || data.length != 16) {
            throw new NullPointerException("Data null or not right length");
        }
        this.mat4x4[0] = data[0];
        this.mat4x4[1] = data[1];
        this.mat4x4[2] = data[2];
        this.mat4x4[3] = data[3];
        this.mat4x4[4] = data[4];
        this.mat4x4[5] = data[5];
        this.mat4x4[6] = data[6];
        this.mat4x4[7] = data[7];
        this.mat4x4[8] = data[8];
        this.mat4x4[9] = data[9];
        this.mat4x4[10] = data[10];
        this.mat4x4[11] = data[11];
        this.mat4x4[12] = data[12];
        this.mat4x4[13] = data[13];
        this.mat4x4[14] = data[14];
        this.mat4x4[15] = data[15];
    }

    @Override
    public Float[] getRow(int j) {
        if (j < 0 || j >= m) {
            throw new IndexOutOfBoundsException();
        }
        Float[] result = new Float[n];
        for (int i = 0; i < n; i++) {
            result[i] = getData(j, i);
        }
        return result;
    }

    @Override
    public MatrixF add(Matrix b) {
        if (b instanceof Matrix4f) {
            return add((Matrix4f) b);
        }
        super.add(b);
        return this;
    }

    public MatrixF add(Matrix4f b) {
        add(b, this);
        return this;
    }

    @Override
    public MatrixF add(Matrix b, Matrix c) {
        if (b instanceof Matrix4f && c instanceof Matrix4f) {
            return add((Matrix4f) b, (Matrix4f) c);
        }
        super.add(b, c);
        return (MatrixF) c;
    }

    public MatrixF add(Matrix4f b, Matrix4f c) {
        c.mat4x4[0] = mat4x4[0] + b.mat4x4[0];
        c.mat4x4[1] = mat4x4[1] + b.mat4x4[1];
        c.mat4x4[2] = mat4x4[2] + b.mat4x4[2];
        c.mat4x4[3] = mat4x4[3] + b.mat4x4[3];
        c.mat4x4[4] = mat4x4[4] + b.mat4x4[4];
        c.mat4x4[5] = mat4x4[5] + b.mat4x4[5];
        c.mat4x4[6] = mat4x4[6] + b.mat4x4[6];
        c.mat4x4[7] = mat4x4[7] + b.mat4x4[7];
        c.mat4x4[8] = mat4x4[8] + b.mat4x4[8];
        c.mat4x4[9] = mat4x4[9] + b.mat4x4[9];
        c.mat4x4[10] = mat4x4[10] + b.mat4x4[10];
        c.mat4x4[11] = mat4x4[11] + b.mat4x4[11];
        c.mat4x4[12] = mat4x4[12] + b.mat4x4[12];
        c.mat4x4[13] = mat4x4[13] + b.mat4x4[13];
        c.mat4x4[14] = mat4x4[14] + b.mat4x4[14];
        c.mat4x4[15] = mat4x4[15] + b.mat4x4[15];
        return this;
    }

    @Override
    public MatrixF subtract(Matrix b) {
        if (b instanceof Matrix4f) {
            return subtract((Matrix4f) b);
        }
        super.subtract(b);
        return this;
    }

    public MatrixF subtract(Matrix4f b) {
        subtract(b, this);
        return this;
    }

    @Override
    public MatrixF subtract(Matrix b, Matrix c) {
        if (b instanceof Matrix4f && c instanceof Matrix4f) {
            return subtract((Matrix4f) b, (Matrix4f) c);
        }
        super.subtract(b, c);
        return (MatrixF) c;
    }

    public MatrixF subtract(Matrix4f b, Matrix4f c) {
        c.mat4x4[0] = mat4x4[0] - b.mat4x4[0];
        c.mat4x4[1] = mat4x4[1] - b.mat4x4[1];
        c.mat4x4[2] = mat4x4[2] - b.mat4x4[2];
        c.mat4x4[3] = mat4x4[3] - b.mat4x4[3];
        c.mat4x4[4] = mat4x4[4] - b.mat4x4[4];
        c.mat4x4[5] = mat4x4[5] - b.mat4x4[5];
        c.mat4x4[6] = mat4x4[6] - b.mat4x4[6];
        c.mat4x4[7] = mat4x4[7] - b.mat4x4[7];
        c.mat4x4[8] = mat4x4[8] - b.mat4x4[8];
        c.mat4x4[9] = mat4x4[9] - b.mat4x4[9];
        c.mat4x4[10] = mat4x4[10] - b.mat4x4[10];
        c.mat4x4[11] = mat4x4[11] - b.mat4x4[11];
        c.mat4x4[12] = mat4x4[12] - b.mat4x4[12];
        c.mat4x4[13] = mat4x4[13] - b.mat4x4[13];
        c.mat4x4[14] = mat4x4[14] - b.mat4x4[14];
        c.mat4x4[15] = mat4x4[15] - b.mat4x4[15];
        return this;
    }

    @Override
    public MatrixF multiply(Matrix b) {
        if (b instanceof Matrix4f) {
            return multiply((Matrix4f) b);
        }
        super.multiply(b);
        return this;
    }

    public Matrix4f multiply(Matrix4f b) {
        multiply4f(b, this);
        return this;
    }

    @Override
    public MatrixF multiply(Matrix b, Matrix c) {
        if (b instanceof Matrix4f && c instanceof Matrix4f) {
            return multiply4f((Matrix4f) b, (Matrix4f) c);
        }
        super.multiply(b, c);
        return (MatrixF) c;
    }

    public Matrix4f multiply4f(Matrix4f b, Matrix4f c) {
        float temp00 = mat4x4[0] * b.mat4x4[0] + mat4x4[1] * b.mat4x4[4] + mat4x4[2] * b.mat4x4[8] + mat4x4[3] * b.mat4x4[12];
        float temp01 = mat4x4[0] * b.mat4x4[1] + mat4x4[1] * b.mat4x4[5] + mat4x4[2] * b.mat4x4[9] + mat4x4[3] * b.mat4x4[13];
        float temp02 = mat4x4[0] * b.mat4x4[2] + mat4x4[1] * b.mat4x4[6] + mat4x4[2] * b.mat4x4[10] + mat4x4[3] * b.mat4x4[14];
        float temp03 = mat4x4[0] * b.mat4x4[3] + mat4x4[1] * b.mat4x4[7] + mat4x4[2] * b.mat4x4[11] + mat4x4[3] * b.mat4x4[15];

        float temp10 = mat4x4[4] * b.mat4x4[0] + mat4x4[5] * b.mat4x4[4] + mat4x4[6] * b.mat4x4[8] + mat4x4[7] * b.mat4x4[12];
        float temp11 = mat4x4[4] * b.mat4x4[1] + mat4x4[5] * b.mat4x4[5] + mat4x4[6] * b.mat4x4[9] + mat4x4[7] * b.mat4x4[13];
        float temp12 = mat4x4[4] * b.mat4x4[2] + mat4x4[5] * b.mat4x4[6] + mat4x4[6] * b.mat4x4[10] + mat4x4[7] * b.mat4x4[14];
        float temp13 = mat4x4[4] * b.mat4x4[3] + mat4x4[5] * b.mat4x4[7] + mat4x4[6] * b.mat4x4[11] + mat4x4[7] * b.mat4x4[15];

        float temp20 = mat4x4[8] * b.mat4x4[0] + mat4x4[9] * b.mat4x4[4] + mat4x4[10] * b.mat4x4[8] + mat4x4[11] * b.mat4x4[12];
        float temp21 = mat4x4[8] * b.mat4x4[1] + mat4x4[9] * b.mat4x4[5] + mat4x4[10] * b.mat4x4[9] + mat4x4[11] * b.mat4x4[13];
        float temp22 = mat4x4[8] * b.mat4x4[2] + mat4x4[9] * b.mat4x4[6] + mat4x4[10] * b.mat4x4[10] + mat4x4[11] * b.mat4x4[14];
        float temp23 = mat4x4[8] * b.mat4x4[3] + mat4x4[9] * b.mat4x4[7] + mat4x4[10] * b.mat4x4[11] + mat4x4[11] * b.mat4x4[15];

        float temp30 = mat4x4[12] * b.mat4x4[0] + mat4x4[13] * b.mat4x4[4] + mat4x4[14] * b.mat4x4[8] + mat4x4[15] * b.mat4x4[12];
        float temp31 = mat4x4[12] * b.mat4x4[1] + mat4x4[13] * b.mat4x4[5] + mat4x4[14] * b.mat4x4[9] + mat4x4[15] * b.mat4x4[13];
        float temp32 = mat4x4[12] * b.mat4x4[2] + mat4x4[13] * b.mat4x4[6] + mat4x4[14] * b.mat4x4[10] + mat4x4[15] * b.mat4x4[14];
        float temp33 = mat4x4[12] * b.mat4x4[3] + mat4x4[13] * b.mat4x4[7] + mat4x4[14] * b.mat4x4[11] + mat4x4[15] * b.mat4x4[15];

        c.mat4x4[0] = temp00;
        c.mat4x4[1] = temp01;
        c.mat4x4[2] = temp02;
        c.mat4x4[3] = temp03;

        c.mat4x4[4] = temp10;
        c.mat4x4[5] = temp11;
        c.mat4x4[6] = temp12;
        c.mat4x4[7] = temp13;

        c.mat4x4[8] = temp20;
        c.mat4x4[9] = temp21;
        c.mat4x4[10] = temp22;
        c.mat4x4[11] = temp23;

        c.mat4x4[12] = temp30;
        c.mat4x4[13] = temp31;
        c.mat4x4[14] = temp32;
        c.mat4x4[15] = temp33;
        return c;
    }

    @Override
    public Matrix4f transpose() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Matrix transpose(Matrix r) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF addScalar(Number v) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF addScalar(Number v, Matrix c) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF subtractScalar(Number v) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF subtractScalar(Number v, Matrix c) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF multiplyScalar(Number v) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF multiplyScalar(Number v, Matrix c) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF divideScalar(Number v) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF divideScalar(Number v, Matrix c) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public VectorF transformVector(VectorF v) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixF invert() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Matrix getSubMatrix(int mi, int mj, int ni, int nj, Matrix r) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Matrix invert(Matrix r) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Matrix solve(Matrix b, Matrix c) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Number determinant(Number r) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void setIdentity() {
        mat4x4[0] = 1;
        mat4x4[1] = 0;
        mat4x4[2] = 0;
        mat4x4[3] = 0;
        mat4x4[4] = 0;
        mat4x4[5] = 1;
        mat4x4[6] = 0;
        mat4x4[7] = 0;
        mat4x4[8] = 0;
        mat4x4[9] = 0;
        mat4x4[10] = 1;
        mat4x4[11] = 0;
        mat4x4[12] = 0;
        mat4x4[13] = 0;
        mat4x4[14] = 0;
        mat4x4[15] = 1;
    }

    public Matrix4f load(FloatBuffer buf) {
        buf.put(mat4x4[0]);
        buf.put(mat4x4[1]);
        buf.put(mat4x4[2]);
        buf.put(mat4x4[3]);
        buf.put(mat4x4[4]);
        buf.put(mat4x4[5]);
        buf.put(mat4x4[6]);
        buf.put(mat4x4[7]);
        buf.put(mat4x4[8]);
        buf.put(mat4x4[9]);
        buf.put(mat4x4[10]);
        buf.put(mat4x4[11]);
        buf.put(mat4x4[12]);
        buf.put(mat4x4[13]);
        buf.put(mat4x4[14]);
        buf.put(mat4x4[15]);
        return this;
    }

    public Matrix4f loadColumnMajor(FloatBuffer buf) {
        buf.put(mat4x4[0]);
        buf.put(mat4x4[4]);
        buf.put(mat4x4[8]);
        buf.put(mat4x4[12]);
        buf.put(mat4x4[1]);
        buf.put(mat4x4[5]);
        buf.put(mat4x4[9]);
        buf.put(mat4x4[13]);
        buf.put(mat4x4[2]);
        buf.put(mat4x4[6]);
        buf.put(mat4x4[10]);
        buf.put(mat4x4[14]);
        buf.put(mat4x4[3]);
        buf.put(mat4x4[7]);
        buf.put(mat4x4[11]);
        buf.put(mat4x4[15]);
        return this;
    }

    public void storeColumnMajor(FloatBuffer buf) {
        mat4x4[0] = buf.get();
        mat4x4[4] = buf.get();
        mat4x4[8] = buf.get();
        mat4x4[12] = buf.get();
        mat4x4[1] = buf.get();
        mat4x4[5] = buf.get();
        mat4x4[9] = buf.get();
        mat4x4[13] = buf.get();
        mat4x4[2] = buf.get();
        mat4x4[6] = buf.get();
        mat4x4[10] = buf.get();
        mat4x4[14] = buf.get();
        mat4x4[3] = buf.get();
        mat4x4[7] = buf.get();
        mat4x4[11] = buf.get();
        mat4x4[15] = buf.get();
    }

    public static Matrix4f zero() {
        return new Matrix4f(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    }

    public static Matrix4f identity() {
        return new Matrix4f(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    }

    public float[] load(float[] output) {
        if (output == null || output.length != 16) {
            throw new NullPointerException("Bad Input");
        }
        output[0] = mat4x4[0];
        output[1] = mat4x4[1];
        output[2] = mat4x4[2];
        output[3] = mat4x4[3];
        output[4] = mat4x4[4];
        output[5] = mat4x4[5];
        output[6] = mat4x4[6];
        output[7] = mat4x4[7];
        output[8] = mat4x4[8];
        output[9] = mat4x4[9];
        output[10] = mat4x4[10];
        output[11] = mat4x4[11];
        output[12] = mat4x4[12];
        output[13] = mat4x4[13];
        output[14] = mat4x4[14];
        output[15] = mat4x4[15];
        return output;
    }
}
