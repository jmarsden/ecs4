/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.math;

/**
 * Abstract size n Vector. Provides all the functionality shared by all
 * Vector classes.
 *
 * <pre>
 *
 * Index     0   1   2     n-1
 *       +---------------------
 * Value |  n1  n2  n3 ...  nN
 *       +---------------------
 *
 * </pre>
 * 
 * @author j.w.marsden@gmail.com
 */
public abstract class Vector {

    /**
     * n dimension
     */
    int n;

    public static final int X, Y, Z;

    static {
        X = 0;
        Y = 1;
        Z = 2;
    }

    public final int getN() {
        return n;
    }

    public abstract Number[] getData();

    public abstract Number getData(int i);

    public abstract void setData(Number[] data);

    public abstract void setData(int i, Number data);

    public abstract Number magnitude();

    public abstract Number magnitudeSquared();

    public Number length() {
        return magnitude();
    }

    public abstract Vector negative();

    public abstract Vector negative(Vector r);

    public boolean isUnit() {
        /**
         * TODO: Test if this is accurate enough and could be optimised to store.
         */
        return length().intValue() == 1;
    }

    public abstract Vector normalise();

    public abstract Vector normalise(Vector r);

    @Override
    public boolean equals(Object b) {
        if(b instanceof Vector) {
            Number[] bData = ((Vector)b).getData();
            for (int i = 0; i < n; i++) {
                if (!getData(i).equals(bData[i])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        for(int i=0;i<n;i++) {
            hash = 67 * hash + getData(i).hashCode();
        }
        return hash;
    }

    public abstract Vector add(Vector b);

    public abstract Vector add(Vector b, Vector c);

    public abstract Vector subtract(Vector b);

    public abstract Vector subtract(Vector b, Vector c);

    public abstract Number dot(Vector b);
    
    public abstract Number dot(Vector b, Number r);

    public abstract Vector cross(Vector b);
    
    public abstract Vector cross(Vector b, Vector c);

    public abstract Vector addScalar(Number v);
    
    public abstract Vector addScalar(Number v, Vector r);

    public abstract Vector subtractScalar(Number v);
    
    public abstract Vector subtractScalar(Number v, Vector r);

    public abstract Vector multiplyScalar(Number v);
    
    public abstract Vector multiplyScalar(Number v, Vector r);

    public abstract Vector divideScalar(Number v);
    
    public abstract Vector divideScalar(Number v, Vector r);
}
