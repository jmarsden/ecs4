/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.math.linear;

import co.cata.math.Matrix;


/**
 * Base Matrix Decomposition class.
 *
 * @author j.w.marsden@gmail.com
 */
public abstract class MatrixDecomposition {

    boolean decomposeFlag = false;
    /**
     * Matrix to be decomposed.
     */
    Matrix a;

    /**
     * Default Constructor.
     * @param a Matrix a.
     */
    public MatrixDecomposition(Matrix a) {
        if (a == null) {
            throw new NullPointerException("Matrix a cannot be null.");
        }
        this.a = a;
    }

    public boolean isDecomposed() {
        return decomposeFlag;
    }

    public void setDecomposeFlag(boolean decomposeFlag) {
        this.decomposeFlag = decomposeFlag;
    }

    public final void decompose() {
        decomposeFlag = handleDecompose();
    }

    /**
     * Abstract Decompose Method.
     */
    protected abstract boolean handleDecompose();
}
