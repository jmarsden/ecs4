/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.utils;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 *
 * @author John
 */
public class ArrayPrinter {

    public static void printArray(float[] a, StringBuilder builder) {
        if (a == null) {
            builder.append("null");
        }
        int length = a.length;
        builder.append('[');
        for (int i = 0; i < (length - 1); i++) {
            float v = a[i];
            if (v >= 0) {
                builder.append(' ');
            }
            builder.append(v).append(',');
        }
        if (length > 0) {
            float v = a[length - 1];
            if (v >= 0) {
                builder.append(' ');
            }
            builder.append(v);
        }
        builder.append(']');
    }

    public static void printArray(float[] a) {
        StringBuilder builder = new StringBuilder();
        printArray(a, builder);
        System.out.println(builder.toString());
    }

    public static String arrayToString(ShortBuffer array) {
        StringBuilder output = new StringBuilder();
        if (array == null) {
            output.append("null");
        } else {
            output.append('[');
            array.rewind();
            for (int i = 0; i < array.capacity() - 1; i++) {
                output.append(array.get()).append(',');
            }
            if(array.capacity() > 0) {
                output.append(array.get());
            }
            output.append(']');
        }
        return output.toString();
    }
    
    public static String arrayToString(FloatBuffer array) {
        StringBuilder output = new StringBuilder();
        if (array == null) {
            output.append("null");
        } else {
            output.append('[');
            array.rewind();
            for (int i = 0; i < array.capacity() - 1; i++) {
                output.append(array.get()).append(',');
            }
            if(array.capacity() > 0) {
                output.append(array.get());
            }
            output.append(']');
        }
        return output.toString();
    }
}
