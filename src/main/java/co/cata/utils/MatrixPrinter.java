/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.utils;

import co.cata.math.Matrix4f;
import java.text.DecimalFormat;

/**
 *
 * @author jmarsden
 */
public class MatrixPrinter {

    static final DecimalFormat DF = new DecimalFormat("#.###");

    public enum MAJOR {
        ROW,
        COLUMN
    }
    
    public static String matrix2String(MAJOR major, float[] matrix) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            builder.append('[');
            for (int j = 0; j < 4; j++) {

                switch (major) {
                    case ROW:
                        builder.append(i).append(j).append(':').append('\t').append(DF.format(matrix[i * 4 + j])).append('\t');
                        break;
                    case COLUMN:
                        builder.append(j).append(i).append(':').append('\t').append(DF.format(matrix[i + j * 4])).append('\t');
                }
            }
            builder.append(']').append('\n');
        }
        return builder.toString();
    }
    
    public static String glMatrix2String(float[] matrix) {
        StringBuilder arrayBuilder = new StringBuilder();

        arrayBuilder.append('[').append(MAJOR.COLUMN).append(':');
        for (int i = 0; i < 15; i++) {
            arrayBuilder.append(matrix[i]).append(',');
        }
        arrayBuilder.append(matrix[15]).append(']');
        
        return arrayBuilder.toString() + '\n' + matrix2String(MAJOR.COLUMN, matrix);
    }
    
    public static String matrix2String(Matrix4f matrix) {
       float[] data = new float[16]; 
       matrix.load(data);
       return matrix2String(MAJOR.ROW, data);
    }
}
