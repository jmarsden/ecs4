/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.debug;

import co.cata.graphics.Vertex;
import co.cata.utils.ArrayPrinter;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;

/**
 *
 * @author John
 */
public class CopySimpleInterleavedExample {
    /*
     1.0f,  1.0f,  1.0f, 1f, 0.0f,  0.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f,
     -1.0f,  1.0f,  1.0f, 1f, 0.0f,  0.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f,
     -1.0f, -1.0f,  1.0f, 1f, 0.0f,  0.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f,
     1.0f, -1.0f,  1.0f, 1f, 0.0f,  0.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f,
     */

    public static void main(String[] args) {
        CopySimpleInterleavedExample example = new CopySimpleInterleavedExample();
        example.start();
    }

    private void start() {
        Vertex vertex1 = new Vertex();
        vertex1.setXYZW(1, 1, 1, 1);
        vertex1.setN(0, 0, 1);
        vertex1.setRGBA(1, 0, 0, 1);

        Vertex vertex2 = new Vertex();
        vertex2.setXYZW(-1, 1, 1, 1);
        vertex2.setN(0, 0, 1);
        vertex2.setRGBA(1, 0, 0, 1);

        Vertex vertex3 = new Vertex();
        vertex3.setXYZW(-1, -1, 1, 1);
        vertex3.setN(0, 0, 1);
        vertex3.setRGBA(1, 0, 0, 1);
        
        Vertex vertex4 = new Vertex(1.0f, -1.0f,  1.0f, 1f, 0.0f,  0.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f);
        
        Vertex[] vertices = new Vertex[]{vertex1, vertex2, vertex3, vertex4};
        ByteBuffer verticesByteBuffer = null;
        // Put each 'Vertex' in one FloatBuffer
        verticesByteBuffer = BufferUtils.createByteBuffer(vertices.length * Vertex.getByteCount(Vertex.POSITION | Vertex.NORMAL | Vertex.COLOR));
        FloatBuffer verticesFloatBuffer = verticesByteBuffer.asFloatBuffer();
        for (int i = 0; i < vertices.length; i++) {
            vertices[i].get(Vertex.POSITION | Vertex.NORMAL | Vertex.COLOR, verticesFloatBuffer);
        }
        verticesFloatBuffer.rewind();

        float[] temp = new float[verticesFloatBuffer.capacity()];
        verticesFloatBuffer.get(temp);

        ArrayPrinter.printArray(temp);

        float[] vertex_data_array = {
            //   x      y      z      nx     ny     nz     r      g      b      a
            // back quad
            1.0f, 1.0f, 1.0f, 1f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            -1.0f, 1.0f, 1.0f, 1f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 1.0f, 1f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            1.0f, -1.0f, 1.0f, 1f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            // front quad
            1.0f, 1.0f, -1.0f, 1f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
            -1.0f, 1.0f, -1.0f, 1f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, -1.0f, 1f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
            1.0f, -1.0f, -1.0f, 1f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
            // left quad
            -1.0f, 1.0f, -1.0f, 1f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, 1.0f, 1f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f, 1f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, -1.0f, 1f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
            // right quad
            1.0f, 1.0f, -1.0f, 1f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 1f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f, 1f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
            1.0f, -1.0f, -1.0f, 1f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
            // top quad
            -1.0f, 1.0f, -1.0f, 1f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
            -1.0f, 1.0f, 1.0f, 1f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 1f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 1.0f, -1.0f, 1f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
            // bottom quad
            -1.0f, -1.0f, -1.0f, 1f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f, 1f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f, 1f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, -1.0f, 1f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f
        };
        ArrayPrinter.printArray(vertex_data_array);
        
        int stride = Vertex.getByteCount(Vertex.POSITION | Vertex.NORMAL | Vertex.COLOR);
        System.out.println("Stride: " + stride);
        
        int positionOffset = 0;
        int normalOffset = Vertex.getByteCount(Vertex.POSITION);
        int colourOffset = Vertex.getByteCount(Vertex.POSITION | Vertex.NORMAL);
        
        System.out.println("Offsets: " + positionOffset + " " + normalOffset + " " + colourOffset);
    }
}
