/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.debug;

import co.cata.ecs.renderer.OrthogonalProjection;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;

/**
 *
 * @author John
 */
public class DumpProjectionIntoBuffer {

    public static void main(String[] args) {
        FloatBuffer matrix44Buffer = BufferUtils.createFloatBuffer(16);
        float left = 0;
        float right = 1;
        float top = 1;
        float bottom = 0;
        float near = 0;
        float far = 1;
        OrthogonalProjection projection = new OrthogonalProjection(left, right, top, bottom, near, far);
        matrix44Buffer.clear();
        projection.get(matrix44Buffer);
        matrix44Buffer.flip();
        float[] output = new float[16];
        matrix44Buffer.get(output);
        System.out.println("To Be Projection Matrix: \n" + matrix2String(output));
        
        matrix44Buffer.clear();
        projection.get(matrix44Buffer);
        matrix44Buffer.flip();
        matrix44Buffer.get(output);
        System.out.println("To Be Projection Matrix: \n" + matrix2String(output));
    }

    public static String matrix2String(float[] matrix) {

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < 4; i++) {
            builder.append('[').append('\t');

            for (int j = 0; j < 4; j++) {
                builder.append(matrix[i * 4 + j]).append('\t');
            }
            builder.append(']').append('\n');

        }

        return builder.toString();

    }
}
