/**
 * Copyright (C) 2015 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.debug.ecs;

import co.cata.ecs.engine.Engine;
import co.cata.ecs.engine.GameObject;
import co.cata.ecs.renderer.GLVersion;
import co.cata.ecs.renderer.Platform;
import co.cata.ecs.renderer.Renderer;
import co.cata.ecs.runtime.ApplicationInterface;
import co.cata.graphics.Vertex;
import co.cata.test.provider.TestApplication;
import co.cata.test.provider.TestApplicationConfiguration;
import co.cata.utils.Color;
import co.cata.utils.PNGDecoder;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author John
 */
public class TestECSRuntimeApplication implements ApplicationInterface {

    public static final String TITLE = "ECS Renderer Tester";
    int width;
    int height;
    int fps;
    boolean paused;
    Engine engine;
    Logger logger;

    public static void main(String[] args) {
        TestECSRuntimeApplication ecsApplication = new TestECSRuntimeApplication();
        TestApplicationConfiguration applicationConfiguration = new TestApplicationConfiguration();
        applicationConfiguration.title = TITLE;
        applicationConfiguration.width = 350;
        applicationConfiguration.height = 350;
        applicationConfiguration.resizable = true;
        applicationConfiguration.backGroundColor = Color.ANTIQUE_WHITE_3;
        applicationConfiguration.forceGLVersion = GLVersion.GL20;
        applicationConfiguration.fpsTarget = 60;

        TestApplication application = new TestApplication(ecsApplication, applicationConfiguration);
        application.start();
    }

    private static ByteBuffer loadIcon(URL url) throws IOException {
        InputStream inputStream = url.openStream();
        try {
            PNGDecoder decoder = new PNGDecoder(inputStream);
            ByteBuffer bytebuf = ByteBuffer.allocateDirect(decoder.getWidth() * decoder.getHeight() * 4);
            decoder.decode(bytebuf, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
            bytebuf.flip();
            return bytebuf;
        } finally {
            inputStream.close();
        }
    }

    public void setIcon(Renderer renderer) {
        URL icon16 = Object.class.getResource("/icon/icon16.png");
        URL icon32 = Object.class.getResource("/icon/icon32.png");
        URL icon128 = Object.class.getResource("/icon/icon128.png");
        ByteBuffer[] icons = null;

        try {
            switch (renderer.getPlatform().platform) {
                case Platform.LINUX:
                    icons = new ByteBuffer[]{loadIcon(icon32)};
                    break;
                case Platform.OSX:
                    icons = new ByteBuffer[]{loadIcon(icon128)};
                    break;
                case Platform.WINDOWS:
                    icons = new ByteBuffer[]{loadIcon(icon16), loadIcon(icon32)};
                    break;
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(TestECSBasic.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (icons != null) {
            //Display.setIcon(icons);
        }
    }

    public void init(Renderer renderer) {
        setIcon(renderer);
        renderer.setFrameRate(60);
        logger = LogManager.getLogger(TestECSBasic.class);
        if (logger.isInfoEnabled()) {
            logger.info("init()");
        }
        String rendererDump = renderer.toString();
        if (logger.isInfoEnabled()) {
            logger.info(rendererDump);
        }

        float left = 0;
        float right = 4;
        float top = 4;
        float bottom = 0;
        float near = 0;
        float far = 1;
        renderer.setOrthoProjection(left, right, top, bottom, near, far);

        engine = new Engine();
        GameObject testObject = engine.createGameObject("Test Game Object");

        testObject.createGeometry();
        testObject.geometry.texture = renderer.createTexture("/assets/images/texture1.png");
        testObject.geometry.texture.init();
        testObject.geometry.texture.load();

        Vertex v0 = new Vertex();
        v0.setXYZ(0f, 1f, 0);
        v0.setRGB(1, 1, 0);
        v0.setST(0, 0);
        v0.setN(0, 0, 1);
        Vertex v1 = new Vertex();
        v1.setXYZ(0f, 0f, 0);
        v1.setRGB(0, 1, 1);
        v1.setST(0, 1);
        v1.setN(0, 0, 1);
        Vertex v2 = new Vertex();
        v2.setXYZ(1f, 0f, 0);
        v2.setRGB(1, 0, 1);
        v2.setST(1, 1);
        v2.setN(0, 0, 1);
        Vertex v3 = new Vertex();
        v3.setXYZ(1f, 1f, 0);
        v3.setRGB(1, 1, 1);
        v3.setST(1, 0);
        v3.setN(0, 0, 1);

        renderer.createDefaultShader(
            ClassLoader.class.getResource("/assets/shaders/test/vertexDefault130.glsl"),
            ClassLoader.class.getResource("/assets/shaders/test/fragmentDefault130.glsl"));

        testObject.geometry.shader = renderer.getDefaultShader();
        testObject.geometry.mesh = renderer.createStaticMesh(renderer.getDefaultShader(), new Vertex[]{v0, v1, v2, v3}, new short[]{0, 1, 2, 2, 3, 0});
        testObject.geometry.mesh.init();
        testObject.geometry.mesh.load();

        testObject.createSpatial();
        testObject.spatial.setTranslation(2, 2);

        engine.dumpState();
    }

    public void start() {
        engine.start();
    }

    public void update(float delta) {
        if (!paused) {
            engine.update(delta);
        }
    }

    public void render(Renderer renderer) {
        engine.render(renderer);
    }

    public void pause() {
        paused = true;
    }

    public void resume() {
        paused = false;
    }

    public void exit() {
    }

    public void reSizeCallback(int width, int height) {
        this.height = height;
        this.width = width;
    }

    public void fpsUpdateCallback(Renderer renderer, int fps) {
        if (this.fps != fps) {
            renderer.getDisplay().setTitle(TITLE + " (" + fps + " fps)");
            this.fps = fps;
        }
    }
}
