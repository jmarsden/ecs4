package co.cata.test.provider;

/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import co.cata.ecs.runtime.ApplicationInterface;
import org.lwjgl.glfw.GLFW;

public class TestApplication {

    long lastFrame;
    int fps;
    long lastFPS;
    boolean pauseFlag;
    ApplicationInterface applicationInterface;
    TestApplicationConfiguration applicationConfiguration;
    TestRenderer renderer;

    public TestApplication(ApplicationInterface applicationInterface, TestApplicationConfiguration applicationConfiguration) {
        if (applicationInterface == null) {
            throw new NullPointerException("applicationInterface cannot be null.");
        }
        if (applicationConfiguration == null) {
            throw new NullPointerException("applicationConfiguration cannot be null.");
        }
        this.applicationInterface = applicationInterface;
        this.applicationConfiguration = applicationConfiguration;
        pauseFlag = false;
    }

    public void start() {
        int width = applicationConfiguration.width;
        int height = applicationConfiguration.height;
        TestDisplay display = new TestDisplay();
        display.setTitle(applicationConfiguration.title);
        display.setResizable(applicationConfiguration.resizable);
        display.init(width, height);

        renderer = display.createRenderer(applicationConfiguration);
        renderer.backgroundColor = applicationConfiguration.backGroundColor;
        renderer.frameRate = applicationConfiguration.fpsTarget;
        renderer.init();

        applicationInterface.reSizeCallback(width, height);
        applicationInterface.init(renderer);
        applicationInterface.start();

        getDelta();
        lastFPS = getTime();

        while (!renderer.display.isCloseRequested()) {
            float delta = getDelta();

            applicationInterface.update(delta);

//            if (!renderer.display.isActive()) {
//                if (!pauseFlag) {
//                    applicationInterface.pause();
//                    pauseFlag = true;
//                }
//            } else {
//                if (pauseFlag) {
//                    applicationInterface.resume();
//                    pauseFlag = false;
//                }
//
//                int newWidth = renderer.display.getWidth();
//                int newHeight = renderer.display.getHeight();
//                if (width != newWidth || height != newHeight) {
//                    width = newWidth;
//                    height = newHeight;
//                    this.renderer.display.reSize(width, height);
//                    applicationInterface.reSizeCallback(width, height);
//                }
                applicationInterface.render(renderer);
//            }
            
            renderer.display.update();
            
            //renderer.display.sync(renderer.frameRate);

            updateFPS();
        }

        applicationInterface.exit();
        renderer.display.destroy();
    }

    /**
     * Calculate how many milliseconds have passed since last frame.
     *
     * @return milliseconds passed since last frame
     */
    public float getDelta() {

        long time = getTime();
        float delta = (float) (time - lastFrame);
        lastFrame = time;
        return delta;
    }

    /**
     * Get the accurate system time
     *
     * @return The system time in milliseconds
     */
    public long getTime() {
        return (long)(GLFW.glfwGetTime() * 1000);
    }
    
    public void updateFPS() {
        if (getTime() - lastFPS > 1000) {
            applicationInterface.fpsUpdateCallback(renderer, fps);
            fps = 0;
            lastFPS += 1000;
        }
        fps++;
    }
}
