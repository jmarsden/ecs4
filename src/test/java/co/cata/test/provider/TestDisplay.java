/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.cata.test.provider;

import co.cata.ecs.provider.LWJGL3Display;
import co.cata.ecs.provider.LWJGL3RendererGL15;
import co.cata.ecs.provider.LWJGL3RendererGL2;
import co.cata.ecs.renderer.GLVersion;
import co.cata.ecs.renderer.Renderer;
import co.cata.ecs.runtime.ApplicationConfiguration;

/**
 *
 * @author John
 */
public class TestDisplay extends LWJGL3Display {

    @Override
    public TestRenderer createRenderer(ApplicationConfiguration configuration) {
        if (!displayCreated) {
            throw new RuntimeException("Must init");
        }
        return new TestRenderer(this);
    }
    
}