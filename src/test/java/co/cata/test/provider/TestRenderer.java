package co.cata.test.provider;

import co.cata.ecs.engine.GameObject;
import co.cata.ecs.provider.FileLWJGLTexture;
import co.cata.ecs.provider.LWJGL3Display;
import co.cata.ecs.provider.LWJGL3InMemoryMeshVAO;
import co.cata.ecs.provider.LWJGL3Renderer;
import co.cata.ecs.provider.StringLWJGLShader;
import co.cata.ecs.renderer.GLSLVersion;
import static co.cata.ecs.renderer.GLSLVersion.GLSL430;
import co.cata.ecs.renderer.Mesh;
import co.cata.ecs.renderer.Shader;
import co.cata.ecs.renderer.ShaderDefaults;
import co.cata.ecs.renderer.Texture;
import co.cata.graphics.Vertex;
import org.lwjgl.opengl.GL11;
import co.cata.math.Matrix4f;
import co.cata.math.Transformation;
import co.cata.utils.ArrayPrinter;
import co.cata.utils.MatrixPrinter;
import co.cata.utils.PNGDecoder;
import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author John
 */
public class TestRenderer extends LWJGL3Renderer {


    boolean firstRenderCycle;

    public TestRenderer(LWJGL3Display display) {
        super(display);
        firstRenderCycle = true;
    }

    @Override
    public void init() {
        GL11.glClearColor(backgroundColor.getRed() / 255F, backgroundColor.getGreen() / 255F, backgroundColor.getBlue() / 255F, 1.0f);

        switch (getGLSLVersion()) {
            case GLSL110:
                throw new RuntimeException("Dont Support " + GLSLVersion.GLSL110);
            case GLSL120:
                throw new RuntimeException("Dont Support " + GLSLVersion.GLSL120);
            case GLSL130:
                if (defaultShader == null) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_130, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_130);
                }
                break;
            case GLSL140:
                if (defaultShader == null) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_140, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_140);
                }
                break;
            case GLSL150:
                if (defaultShader == null) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_150, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_150);
                }
                break;
            case GLSL330:
            case GLSL400:
            case GLSL410:
            case GLSL420:
            case GLSL430:
                if (defaultShader == null) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_150, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_150);
                }
                break;

            default:
                if (getGLSLVersion().getVersion() > GLSL430.getVersion()) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_150, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_150);
                }

                throw new RuntimeException("Dont Support " + getGLSLVersion());
        }

        if (!defaultShader.isInitialized()) {
            defaultShader.init();
        }
        if (!defaultShader.isLoaded()) {
            defaultShader.load();
        }
        if (defaultShader.inError()) {
            throw new RuntimeException("Default Shader No Compile: " + defaultShader.getVertexCompileError() + " " + defaultShader.getFragmentCompileError() + " " + defaultShader.getLinkError());
        }

//        ecsProjectionMatrix = new Matrix4f();
//        ecsViewMatrix = Matrix4f.identity();
//        ecsModelMatrix = Matrix4f.identity();

        /* **
         * Debug
         */
        //setupShaders();
        //shader = getDefaultShader();
        //setupTextures();
        //setupQuad();
    }

    public StringLWJGLShader createShader(String vertexSource, String fragmentSource) {
        return new StringLWJGLShader(vertexSource, fragmentSource);
    }

    public StringLWJGLShader createShader(URL vertexURL, URL fragmentURL) {
        return new StringLWJGLShader(ClassLoader.class.getResource("/assets/shaders/test/vertexDefault130.glsl"),
                ClassLoader.class.getResource("/assets/shaders/test/fragmentDefault130.glsl"));
    }

    public void releaseShader(Shader shader) {
        shader.release();
    }

    public StringLWJGLShader createDefaultShader(String vertexSource, String fragmentSource) {
        if (defaultShader != null) {
            defaultShader.release();
        }
        defaultShader = createShader(vertexSource, fragmentSource);
        return defaultShader;
    }

    public StringLWJGLShader createDefaultShader(URL vertexURL, URL fragmentURL) {
        if (defaultShader != null) {
            defaultShader.release();
        }
        defaultShader = createShader(vertexURL, fragmentURL);
        return defaultShader;
    }

    public StringLWJGLShader getDefaultShader() {
        return defaultShader;
    }

    public void completeRender() {

        if (!defaultShader.isLoaded()) {
            defaultShader.load();
        }

        if (skipRender) {
            batch.clear();
            lock = false;
            return;
        }

        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

        projection.load(projectionMatrix);

        camera.load(viewMatrix);

        for (GameObject gameObject : batch.queue) {

            if (gameObject.geometry == null || !gameObject.geometry.isValid()) {
                continue;
            }
            if (gameObject.spatial == null) {
                continue;
            }

            Shader shader = getDefaultShader();
            Texture texture = gameObject.geometry.texture;
            Mesh mesh = gameObject.geometry.mesh;

            Transformation transformation = gameObject.getWorld();
            transformation.load(modelMatrix);

            texture.enable();
            {
                shader.enable();
                {

                    if (firstRenderCycle) {
                        System.out.println("First Render Cycle Debug");
                        System.out.println("Shader:" + shader.getState());
                        System.out.println("Texture:" + texture.toString());
                        System.out.println("Mesh:" + mesh.dumpState());
                        System.out.println("ECS Othogonal Projection Matrix: \n" + MatrixPrinter.matrix2String(projectionMatrix));
                        System.out.println("ECS View Matrix: \n" + MatrixPrinter.matrix2String(viewMatrix));
                        System.out.println("ECS Model Matrix: \n" + MatrixPrinter.matrix2String(modelMatrix));
                        firstRenderCycle = false;
                    }

                    shader.loadUniform(shader.getUniformLocation("projectionMatrix"), projectionMatrix);
                    shader.loadUniform(shader.getUniformLocation("viewMatrix"), viewMatrix);
                    shader.loadUniform(shader.getUniformLocation("modelMatrix"), modelMatrix);

                    mesh.enable(shader);
                    GL11.glDrawElements(GL11.GL_TRIANGLES, mesh.getIndicesCount(), GL11.GL_UNSIGNED_SHORT, 0);
                    mesh.disable(shader);
                }
                shader.disable();
            }
            texture.disable();
        }

        batch.clear();
        lock = false;
    }

  
    private int loadPNGTexture(String filename, int textureUnit) {
        ByteBuffer buf = null;
        int tWidth = 0;
        int tHeight = 0;

        try {
            // Open the PNG file as an InputStream
            URL file = ClassLoader.class.getResource(filename);

            // Link the PNG decoder to this stream
            PNGDecoder decoder = new PNGDecoder(file.openStream());

            // Get the width and height of the texture
            tWidth = decoder.getWidth();
            tHeight = decoder.getHeight();

            // Decode the PNG file in a ByteBuffer
            buf = ByteBuffer.allocateDirect(
                    4 * decoder.getWidth() * decoder.getHeight());
            decoder.decode(buf, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
            buf.flip();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        // Create a new texture object in memory and bind it
        int texId = GL11.glGenTextures();
        GL13.glActiveTexture(textureUnit);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texId);

        // All RGB bytes are aligned to each other and each component is 1 byte
        GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);

        // Upload the texture data and generate mip maps (for scaling)
        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, tWidth, tHeight, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buf);
        GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);

        // Setup the ST coordinate system
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);

        // Setup what to do when the texture has to be scaled
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);

        return texId;
    }
}
